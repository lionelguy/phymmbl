#!/usr/bin/perl

=head1 SYNOPSIS

scoreReads.pl - Score reads or sequences using 

=head1 USAGE

scoreReads.pl [-d|--database <path>] [-t|--threads <int>] <fasta_file>

=head1 INPUT

=head2 [-d|--database <path>]

The path where the phymmbl database is/will be stored. This includes logs, genomeData, taxonomyData and blastData folders.

=head2 [-t|--threads <int>]

Number of threads to use. Minimum is 2 (default).

=head1 AUTHOR

Arthur Brady
Lionel Guy (lionel.guy@imbim.uu.se)

=head1 DATE

2015-06-25 for the revised version by Lionel Guy

=head1 COPYRIGHT

Copyright (c) 2009-2012 Arthur Brady; 
          (c) 2015 Arthur Brady, Lionel Guy

=head1 LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

$| = 1;
use strict;
use warnings;
use Pod::Usage;
use Cwd;
use Getopt::Long;
use FindBin qw/$Bin/;
use lib "$Bin/scripts";
use POE qw(Wheel::Run Filter::Reference);

my $debug = 0;
my $datapath = getcwd;
my $help;
my $nthreads = 2;
GetOptions('d|database=s' => \$datapath,
	   't|threads=i'  => \$nthreads,
	   'h|help'       => \$help,
	  );

pod2usage(-exitval => 1, -verbose => 2) if $help;
# Nthreads can be only 2 or more
$nthreads = 2 if ($nthreads < 2);


my ($dataFile) = @ARGV;
$dataFile = &unixEscape($dataFile);

my $outputPrefix = $dataFile;
$outputPrefix =~ s/\//\_/g;
$outputPrefix =~ s/\./\_/g;
die("Usage: $0 <file containing query reads>\n") if not $dataFile;

# Date variable for progress reports.
my $date;
my $blastFile;
my $blastScore;
my $blastMatch;
my @taxaToParse = qw/ genus family order class phylum /;

################################################################################
#
# Save the query lengths for later confidence-score computation.
#
################################################################################
print "Scanning query lengths...";
open IN, "<$dataFile" or die("Can't open $dataFile for reading.\n");
my $currentID = '';
my $currentLength = 0;
my $first = 1;
my $queryLengths = {};
my @readIDs = ();
while ( my $line = <IN> ) {
  if ( $line =~ /^>(\S+)/ and $first ) {
    $currentID = $1;
    push @readIDs, $currentID;
    $first = 0;
  } elsif ( $line =~ /^>(\S+)/ ) {
    my $tempID = $1;
    $queryLengths->{$currentID} = $currentLength;
    $currentLength = 0;
    $currentID = $tempID;
    push @readIDs, $currentID;
  } elsif ( $line =~ /^(\S+)$/ ) {
    my $data = $1;
    $currentLength += length($data);
  }
}
$queryLengths->{$currentID} = $currentLength;
$date = `date`;
chomp $date;
print "done.  [$date]\n\n";

################################################################################
#
# Set / check BLAST binaries.
#
################################################################################
my $locateBlastBinary = `which makeblastdb 2>&1`;
if ( $locateBlastBinary =~ /no\s+makeblastdb\s+in/ 
     or $locateBlastBinary =~ /command\s+not\s+found/i ) {
  die("FATAL: You must have a local copy of the BLAST software installed\nand accessible via your \"PATH\" environment variable.  Please see\nthe README file for details.\n\n");
}

################################################################################
#
# Grab the list of ICMs.
#
################################################################################
print "Scanning ICM list...";

# ICM list: 1: genomeData.
opendir DOT, "$datapath/genomeData" 
  or die("Can't open $datapath/genomeData for scanning.\n");
my @dirs = sort { $a cmp $b } grep { !/^(\.)|(_userAdded)/ } readdir DOT;
closedir DOT;
our @ICMs;
foreach my $dir ( @dirs ) {
  if ( -d "$datapath/genomeData/$dir" ) {
    &scanDir("$datapath/genomeData/$dir", \@ICMs);
  }
}


# ICM list: 2: genomeData/_userAdded, if it exists.
my $userDir = "$datapath/genomeData/_userAdded";
if ( -e $userDir ) {
  opendir DOT, $userDir or die("Can't open $userDir for scanning.\n");
  my @dirs = sort { $a cmp $b } grep { !/^(\.)|(_userAdded)/ } readdir DOT;
  closedir DOT;
  foreach my $dir ( @dirs ) {
    if ( -d "$userDir/$dir" ) {
      &scanDir("$userDir/$dir", \@ICMs);
    }
  }
}

# Sort ICM list
@ICMs = sort { $a cmp $b } @ICMs;

$date = `date`;
chomp $date;
print "done.  [$date]\n\n";

################################################################################
#
# Create a reverse-complement copy of the query file.
#
################################################################################
print "Creating reverse complement of $dataFile...";
system("$Bin/scripts/revCompFASTA.pl $dataFile");
my $revDataFile = $dataFile;
$revDataFile =~ /(\.[^\.]+)$/;
my $extension = $1;
$revDataFile =~ s/$extension$/\.revComp$extension/;
my $fileCheck = $revDataFile;
$fileCheck =~ s/\\//g;
if ( not -e $fileCheck ) {
  die("FATAL: File renaming problem [revCompFASTA.pl]: could not detect revComp file \"$fileCheck\".\n");
}
$date = `date`;
chomp $date;
print "done.  [$date]\n\n";

################################################################################
#
# Read organism names so we can generate results files comparable to what BLAST will output.
#
################################################################################
print "Loading taxonomy...";
# Accession scan 1: RefSeq orgs.
my $accFile = "$datapath/taxonomyData/0_accessionMap/accessionMap.txt";
open IN, "<$accFile" or die("Can't open $accFile for reading.\n");
my $speciesDirName = {};
while ( my $line = <IN> ) {
  chomp $line;
  (my $orgName, my $prefix, my $seqType, my $desc) = split(/\t/, $line);
  $speciesDirName->{$prefix} = $orgName;
}
close IN;
# Accession scan 2: user-added orgs, if they exist.
$accFile = "$datapath/taxonomyData/0_accessionMap/accessionMap_userAdded.txt";
if ( -e $accFile ) {
  open IN, "<$accFile" or die("Can't open $accFile for reading.\n");
  while ( my $line = <IN> ) {
    chomp $line;
    (my $orgName, my $prefix) = split(/\t/, $line);
    $speciesDirName->{$prefix} = $orgName;
  }
  close IN;
}

################################################################################
#
# Load full taxonomic metadata for all organisms in the database.
#
################################################################################
# Taxonomy scan 1: RefSeq organisms.
my $taxFile = "$datapath/taxonomyData/3_parsedTaxData/distributionOfTaxa.txt";
open IN, "<$taxFile" or die("Can't open $taxFile for reading.\n");
my $tax = {};
while ( my $line = <IN> ) {
  if ( $line =~ /^\S/ ) {
    chomp $line;
    (my $taxType, my $taxVal, my $prefixAndSpecies, my $dirName) = 
      split(/\t/, $line);
    if ( $taxType eq 'phylum' or $taxType eq 'class' or $taxType eq 'order' 
	 or $taxType eq 'family' or $taxType eq 'genus' ) {
      $tax->{$dirName}->{$taxType} = $taxVal;
    }
  }
}
close IN;

# Taxonomy scan 2: user-added organisms, if they exist.
$taxFile = 
  "$datapath/taxonomyData/3_parsedTaxData/distributionOfTaxa_userAdded.txt";
if ( -e $taxFile ) {
  open IN, "<$taxFile" or die("Can't open $taxFile for reading.\n");
  while ( my $line = <IN> ) {
    if ( $line =~ /^\S/ ) {
      chomp $line;
      (my $taxType, my $taxVal, my $prefixAndSpecies, my $dirName) = 
	split(/\t/, $line);
      if ( $taxType eq 'phylum' or $taxType eq 'class' or $taxType eq 'order' 
	   or $taxType eq 'family' or $taxType eq 'genus' ) {
	$tax->{$dirName}->{$taxType} = $taxVal;
      }
    }
  }
}
$date = `date`;
chomp $date;
print "done.  [$date]\n\n";

################################################################################
#
# Score the query data (using both forward and reverse directions) with Phymm.
#
################################################################################
my $score = {};
my $topScoringICM = {};
my $icmCount = @ICMs;
our $icmsFinished = 0;
my $logDir = "$datapath/logs/scoring";
system("mkdir -p $logDir");
my $logFile = "$logDir/$outputPrefix\_logFile.txt";
$logFile =~ s/\\//g;
open LOG, ">$logFile" or die("Can't open $logFile for writing.\n");
print LOG "Scoring reads with Phymm [$date]...\n\n";
print "Scoring reads with Phymm (logfile is \"$logFile\")...\n\n";

# Create and run the POE: the tasks are taken directly from @ICM
our @tasks = @ICMs;
POE::Session->create(
  inline_states => {
  _start      => \&start_tasks,
  next_task   => \&start_tasks,
  task_result => \&handle_task_result,
  task_done   => \&handle_task_done,
  task_debug  => \&handle_task_debug,
  sig_child   => \&sig_child,
});
$poe_kernel->run();

# Concatenate temp result files in the right order
&consolidateRawScores;
$date = `date`;
chomp $date;

################################################################################
#
# Write the Phymm output.
#
################################################################################
my $phymmOut = "results.01.phymm_$outputPrefix.txt";
$phymmOut =~ s/\\//g;
open OUT, ">$phymmOut" or die("Can't open $phymmOut for writing.\n");
print OUT "QUERY_ID\tBEST_MATCH\tSCORE\tGENUS\tFAMILY\tORDER\tCLASS\tPHYLUM\n";
foreach my $queryID ( sort { $a cmp $b } keys %$score ) {
  my $spName = $speciesDirName->{$topScoringICM->{$queryID}};
  print OUT "$queryID\t$spName\t$score->{$queryID}\t$tax->{$spName}->{'genus'}\t$tax->{$spName}->{'family'}\t$tax->{$spName}->{'order'}\t$tax->{$spName}->{'class'}\t$tax->{$spName}->{'phylum'}\n";
}
close OUT;
system("rm errFile_$outputPrefix.txt") unless $debug;
system("rm $revDataFile") == 0 or die "Cannot remove $revDataFile: $!\n";
$date = `date`;
chomp $date;
print LOG "\n...done.  [$date]\n";
print "\n...done.  [$date]\n";

################################################################################
#
# Score the query data using BLAST.
#
################################################################################
print "Scoring reads with BLAST...";
print LOG "Scoring reads with BLAST...";
my $blastDBLoc = "$datapath/blastData";
# chomp $blastDBLoc;
# $blastDBLoc =~ s/\/+$//;
# $blastDBLoc .= '/blastData';
my $blastCmd = "blastn -query $dataFile -out rawBlastOutput_$outputPrefix.txt -db $blastDBLoc/phymm_BLAST_DB -num_threads $nthreads -outfmt 7";
system($blastCmd);
system("rm -f error.log");

################################################################################
#
# Parse the raw BLAST results file to generate a best-hit list.
#
################################################################################
$blastFile = "rawBlastOutput_$outputPrefix.txt";
$blastFile =~ s/\\//g;
$blastScore = {};
$blastMatch = {};
open IN, "<$blastFile" or die("Can't open $blastFile for reading.\n");
while ( my $line = <IN> ) {
  if ( $line !~ /^#/ ) {
    chomp $line;
    my @fields = split(/\t/, $line);
    my $queryID = $fields[0];
    my $matchName = $fields[1];
    $matchName =~ s/\.\d+$//;
    my $currentScore = $fields[10];
    if ( not $blastScore->{$queryID} 
	 or $blastScore->{$queryID} > $currentScore ) {
      $blastScore->{$queryID} = $currentScore;
      $blastMatch->{$queryID} = $matchName;
    }
  }
}
close IN;
my $blastOut = "results.02.blast_$outputPrefix.txt";
$blastOut =~ s/\\//g;
open OUT, ">$blastOut" or die("Can't open $blastOut for writing.\n");
print OUT "QUERY_ID\tBEST_MATCH\tSCORE\tGENUS\tFAMILY\tORDER\tCLASS\tPHYLUM\n";
foreach my $queryID ( sort { $a cmp $b } keys %$blastScore ) {
  my $spName = $blastMatch->{$queryID};
  print OUT "$queryID\t$spName\t$blastScore->{$queryID}\t$tax->{$spName}->{'genus'}\t$tax->{$spName}->{'family'}\t$tax->{$spName}->{'order'}\t$tax->{$spName}->{'class'}\t$tax->{$spName}->{'phylum'}\n";
}
close OUT;
$date = `date`;
chomp $date;
print "done.  [$date]\n\n";
print LOG "done.  [$date]\n\n";
################################################################################
#
# Combine Phymm and BLAST scores to generate PhymmBL predictions.
#
################################################################################
print "Combining scores...\n\n";
print LOG "Combining scores...\n\n";
# Read the raw BLAST E-values.
$blastFile = "rawBlastOutput_$outputPrefix.txt";
$blastFile =~ s/\\//g;
$blastScore = {};
open IN, "<$blastFile" or die("Can't open $blastFile for reading.\n");
$date = `date`;
chomp $date;
print "   Scanning BLAST output [$date]...";
print LOG "   Scanning BLAST output [$date]...";
while ( my $line = <IN> ) {
  if ( $line !~ /^#/ ) {
    chomp $line;
    my @fields = split(/\t/, $line);
    my $queryID = $fields[0];
    my $matchName = $fields[1];
    $matchName =~ s/\.\d+$//;
    my $currentScore = $fields[10];
    if ( not $blastScore->{$queryID}->{$matchName} 
	 or $blastScore->{$queryID}->{$matchName} > $currentScore ) {
      $blastScore->{$queryID}->{$matchName} = $currentScore;
    }
  }
}
close IN;
$date = `date`;
chomp $date;
print "done.  [$date]\n";
print LOG "done.  [$date]\n";

# Read the raw Phymm scores.
my $phymmFile = "rawPhymmOutput_$outputPrefix.txt";
$phymmFile =~ s/\\//g;
open IN, "<$phymmFile" or die("Can't open $phymmFile for reading.\n");
$date = `date`;
chomp $date;
print "   Scanning Phymm output [$date]...";
print LOG "   Scanning Phymm output [$date]...";

# 1: Create an ordered array of ICMs (and metadata for those ICMs)
# using the map which has been saved in the first section of the
# rawPhymmOutput file.
my @indexedICMs = ();
while ( my $line = <IN> ) {
  chomp $line;
  if ( $line =~ /END\_ICM/ ) {
    last;
  } elsif ( $line !~ /^BEGIN\_ICM/ ) {
    if ( $line =~ /^(\S+)$/ ) {
      my $lineText = $1;
      $lineText =~ /([^\/]+)\/[^\/]+\.icm$/;
      my $tempOrgName = $1;
      push @indexedICMs, $tempOrgName;
    } else {
      die("\n   FATAL: Bad filename recorded for ICM: \"$line\"; \n");
    }
  }
}

# 2: Save the ordered query IDs from the next section of the rawPhymmOutput file
my @indexedReads = ();
while ( my $line = <IN> ) {
  chomp $line;
  if ( $line =~ /END\_READID/ ) {
    last;
  } elsif ( $line !~ /BEGIN\_READID/ ) {
    if ( $line =~ /^(\S+)$/ ) {
      my $readID = $1;
      push @indexedReads, $readID;
    } else {
      die("\n   FATAL: Bad read ID: \"$line\"\n");
    }
  }
}

# 3: Scan the stored ICM scores and save the best score assigned to each read
# by (any one of the [possibly multiple] ICMs assigned to) each organism.
# Format: 
# $phymmScore->{$query_ID}->{$org_dirName} = $bestNumericScoreForThatOrg;
my $phymmScore = {};
my $currentIcmIndex = 0;
while ( my $line = <IN> ) {
  if ( $line =~ /END\_DATA\_MATRIX/ ) {
    last;
  } elsif ( $line !~ /BEGIN\_DATA\_MATRIX/ ) {
    chomp $line;
    my @currentICMScores = split(/\t/, $line);
    my $currentOrg = $indexedICMs[$currentIcmIndex];
    for ( my $currentReadIndex = 0; $currentReadIndex <= $#indexedReads; 
	  $currentReadIndex++ ) {
      my $currentScore = $currentICMScores[$currentReadIndex];
      my $currentReadID = $indexedReads[$currentReadIndex];
      # If we haven't yet scored this read with an ICM from this organism, 
      # save the current score as the best seen so far.
      if ( not $phymmScore->{$currentReadID}->{$currentOrg} ) {
	$phymmScore->{$currentReadID}->{$currentOrg} = $currentScore;
      } elsif ( $phymmScore->{$currentReadID}->{$currentOrg} < $currentScore ) {
	# If we have scored this read with some other ICM from this organism, 
	# but this current score is better, than replace the existing score 
	# with the current one as the best seen so far.
	$phymmScore->{$currentReadID}->{$currentOrg} = $currentScore;
      }
    }
    $currentIcmIndex++;
  }
}
close IN;
$date = `date`;
chomp $date;
print "done.  [$date]\n";
print LOG "done.  [$date]\n";

# Iterate on each query ID.  Phymm will score everything;
# BLAST will (very occasionally) turn up no matches, so we
# use the $phymmScore keys as the reference index list.
#
# Combine scores: the formula is [ICM score] + [1.2 * (4 - log(BLAST E-value))].
my $combinedScore = {};
my $combinedMatch = {};
$date = `date`;
chomp $date;
print "   Mixing scores [$date]...";
print LOG "   Mixing scores [$date]...";
foreach my $queryID ( keys %$phymmScore ) {
  # Check each match.
  foreach my $matchName ( keys %{$phymmScore->{$queryID}} ) {
    # Make sure there's a corresponding BLAST score.
    if ( not $blastScore->{$queryID} 
	 or not $blastScore->{$queryID}->{$matchName} ) {
      # If there isn't, just find the best Phymm score.
      if ( not $combinedScore->{$queryID} ) {
	$combinedScore->{$queryID} = $phymmScore->{$queryID}->{$matchName};
	$combinedMatch->{$queryID} = $matchName;
      } elsif ( $combinedScore->{$queryID} 
		< $phymmScore->{$queryID}->{$matchName} ) {
	$combinedScore->{$queryID} = $phymmScore->{$queryID}->{$matchName};
	$combinedMatch->{$queryID} = $matchName;
      }
    } else {
      # There is a corresponding BLAST score: compute the combined score 
      # and compare.
      my $tempScore = $phymmScore->{$queryID}->{$matchName};
      if ( $blastScore->{$queryID}->{$matchName} == 0 ) {
	$tempScore += 1.2 * (4 - log(1e-180));
      } else {
	$tempScore += 1.2 * (4 - log($blastScore->{$queryID}->{$matchName}));
      }
      # Update the best-hit record for this query if we've got
      # something better than something we've seen so far.
      if ( not $combinedScore->{$queryID} 
	   or $tempScore > $combinedScore->{$queryID} ) {
	$combinedScore->{$queryID} = $tempScore;
	$combinedMatch->{$queryID} = $matchName;
      }
    } # end if ( there's a BLAST score for this query and this matching reference organism )
   } # end foreach ( reference organism to which this query was compared )
} # end foreach ( query key in the $phymmScore hash )
$date = `date`;
chomp $date;
print "done.  [$date]\n";
print LOG "done.  [$date]\n";
################################################################################
# 
# Output the combined (i.e., PhymmBL) results.
# 
################################################################################
my $combinedOut = 'results.03.phymmBL_' . $outputPrefix . '.txt';
$combinedOut =~ s/\\//g;
$date = `date`;
chomp $date;
print "   Writing PhymmBL results to $combinedOut [$date]...\n\n";
print LOG "   Writing PhymmBL results to $combinedOut [$date]...\n\n";
open OUT, ">$combinedOut" or die("Can't open $combinedOut for writing.\n");
print OUT "QUERY_ID\tBEST_MATCH\tSCORE\tGENUS\tGENUS_CONF\tFAMILY\tFAMILY_CONF\tORDER\tORDER_CONF\tCLASS\tCLASS_CONF\tPHYLUM\tPHYLUM_CONF\n";
foreach my $queryID ( sort { $a cmp $b } keys %$combinedScore ) {
  my $matchName = $combinedMatch->{$queryID};
  print OUT "$queryID\t$matchName\t$combinedScore->{$queryID}";
  foreach my $level (@taxaToParse){
    my $taxon = '';
    $taxon = $tax->{$matchName}->{$level} if ($tax->{$matchName}->{$level});
    my $confidence = &estimateAccuracy($level, $queryLengths->{$queryID}, 
				       $combinedScore->{$queryID});
    printf OUT "\t$taxon\t%.3f", $confidence;
  }
  print OUT "\n";
}
close OUT;
$date = `date`;
chomp $date;
print "done.  [$date]\n\n";
print LOG "done.  [$date]\n\n";
close LOG;

################################################################################
#
# SUBROUTINES
#
################################################################################
# Run one scoring process
sub runScore {
  my ($ICM) = @_;
  my $filter = POE::Filter::Reference->new();
  my $icmPrefix = $ICM;
  $icmPrefix =~ s/^.+\/([^\/]+)$/$1/;
  $icmPrefix =~ s/\.icm$//;
  my $fullScore = {};
  my $command = "($Bin/scripts/icmCode/bin/simple-score -N $ICM < $dataFile" .
    " > tempFwd_${outputPrefix}_$icmPrefix.txt) 2>> errFile_${outputPrefix}.txt";
  system($command) == 0 or die "Something wrong happened while running: " . 
    $command . "\n$?";
  my $inFile = "tempFwd_${outputPrefix}_$icmPrefix.txt";
  $inFile =~ s/\\//g;
  open IN, "<$inFile" or die("Can't open $inFile for reading.\n");
  while ( my $line = <IN> ) {
    if ( $line =~ /(\S+)\s+(\S+)/ ) {
      my $queryID = $1;
      my $queryScore = $2;
      $fullScore->{$queryID}->{$icmPrefix} = $queryScore;
      if ( not $score->{$queryID} or $queryScore > $score->{$queryID} ) {
	$score->{$queryID} = $queryScore;
	$topScoringICM->{$queryID} = $icmPrefix;
      }
    }
  }
  close IN;
  $command = "($Bin/scripts/icmCode/bin/simple-score -N $ICM < $dataFile" .
    " > tempRev_${outputPrefix}_$icmPrefix.txt) 2>> errFile_${outputPrefix}.txt";
  system($command) == 0 or die "Something wrong happened while running: " . 
    $command . "\n$?";
  $inFile = "tempRev_${outputPrefix}_$icmPrefix.txt";
  $inFile =~ s/\\//g;
  open IN, "<$inFile" or die("Can't open $inFile for reading.\n");
  while ( my $line = <IN> ) {
    if ( $line =~ /(\S+)\s+(\S+)/ ) {
      my $queryID = $1;
      my $queryScore = $2;
      if ( $queryScore > $fullScore->{$queryID}->{$icmPrefix} ) {
	$fullScore->{$queryID}->{$icmPrefix} = $queryScore;
      }
      if ( not $score->{$queryID} or $queryScore > $score->{$queryID} ) {
	$score->{$queryID} = $queryScore;
	$topScoringICM->{$queryID} = $icmPrefix;
      }
    }
  }
  close IN;
  open OUT, '>', "tempRes_${outputPrefix}_$icmPrefix.txt" 
    or die("Can't open tempRes_${outputPrefix}_$icmPrefix.txt for writing.\n");
  my $firstDataPoint = 1;
  foreach my $queryID ( @readIDs ) {
    if ( $firstDataPoint ) {
      print OUT "$fullScore->{$queryID}->{$icmPrefix}";
      $firstDataPoint = 0;
    } else {
      print OUT "\t$fullScore->{$queryID}->{$icmPrefix}";
    }
  }
  print OUT "\n";
  close OUT;
  # Keep user updated
  my %result = ( finished => 1 );
  my $output = $filter->put( [\%result] );
  print @$output;
  system("rm tempFwd_${outputPrefix}_$icmPrefix.txt");
  system("rm tempRev_${outputPrefix}_$icmPrefix.txt");
}

# Consolidate results of individual ICMs into a single file
sub consolidateRawScores {
  my $rawPhymmFile = 'rawPhymmOutput_' . $outputPrefix . '.txt';
  $rawPhymmFile =~ s/\\//g;
  open OUT, ">$rawPhymmFile" or die("Can't open $rawPhymmFile for writing.\n");
  print OUT "BEGIN_ICM_LIST\n";
  foreach my $ICM ( @ICMs ) {
    print OUT "$ICM\n";
  }
  print OUT "END_ICM_LIST\nBEGIN_READID_LIST\n";
  foreach my $readID ( @readIDs ) {
    print OUT "$readID\n";
  }
  print OUT "END_READID_LIST\nBEGIN_DATA_MATRIX\n";
  foreach my $ICM ( @ICMs ) {
    my $icmPrefix = $ICM;
    $icmPrefix =~ s/^.+\/([^\/]+)$/$1/;
    $icmPrefix =~ s/\.icm$//;
    # Concatenate file
    open IN, '<', "tempRes_${outputPrefix}_$icmPrefix.txt" 
      or die "Could not open tempRes_${outputPrefix}_$icmPrefix.txt: $!";
    my @lines = <IN>;
    close IN;
    print OUT @lines;
    system("rm tempRes_${outputPrefix}_$icmPrefix.txt") == 0 
      or die "Cannot remove $revDataFile: $!\n";
  }
  print OUT "END_DATA_MATRIX\n";
  close OUT;
}
# POE subs
sub start_tasks {
  my ($kernel, $heap) = @_[KERNEL, HEAP];
  while (keys( %{$heap->{task}} ) < $nthreads){
    print scalar(keys(%{$heap->{task}})) . " tasks running\n" if $debug;
    my $next_icm = shift @tasks;
    # if no more tasks be sure to wait unil all tasks are finished
    last unless $next_icm;
    print "Starting task for " . $next_icm . "...\n" if $debug;
    my $task = POE::Wheel::Run->new(
      Program      => sub { runScore($next_icm) },
      StdoutFilter => POE::Filter::Reference->new(),
      StdoutEvent  => "task_result",
      StderrEvent  => "task_debug",
      CloseEvent   => "task_done",
    );
    $heap->{task}->{$task->ID} = $task;
    $kernel->sig_child($task->PID, "sig_child");
  }
}
sub handle_task_result {
  my $result = $_[ARG0];
  $icmsFinished += $result->{finished};
  if ( $icmsFinished % 50 == 0 ) {
    my $date = `date`;
    chomp $date;
    print LOG "   ...finished scoring read set with $icmsFinished / $icmCount" .
      " ICMs [$date]...\n";
    print "   ...finished scoring read set with $icmsFinished / $icmCount" . 
      " ICMs [$date]...\n";
  }
}
sub handle_task_done {
  my ($kernel, $heap, $task_id) = @_[KERNEL, HEAP, ARG0];
  delete $heap->{task}->{$task_id};
  $kernel->yield("next_task");
}
sub handle_task_debug {
  my $result = $_[ARG0];
  print "Debug: $result\n";
}
sub sig_child {
  my ($heap, $sig, $pid, $exit_val) = @_[HEAP, ARG0, ARG1, ARG2];
  my $details = delete $heap->{$pid};
  #warn "$$: Child $pid exited with exit val $exit_val";
}


# Scan a directory for all the .icm files in it, and save the local path for each into the array we received as an argument.
sub scanDir {
  my $dir = shift;
  my $arrayRef = shift;
  opendir DOT, $dir or die("Can't open $dir for scanning.\n");
  my @files = sort { $a cmp $b } grep { /\.icm$/ } readdir DOT;
  closedir DOT;
  foreach my $file ( @files ) {
    # Extra sanity check.  If any of the ICMs is zero-length, something's 
    # gone wrong.
    if ( -s "$dir/$file" == 0 ) {
      die("FATAL: ICM file \"$dir/$file\" is zero-length.  Something\nwent wrong during construction; please re-run the phymmSetup.pl to fix.\n\n");
    }
    push @$arrayRef, "$dir/$file";
  }
}

# Generate confidence estimates from the two-variable polynomials fitted 
# to experimental accuracy data.
sub estimateAccuracy {
  my $clade = shift;
  my $readLength = shift;
  my $phymmblScore = shift;
  my $retVal;
  if ( $readLength < 1 ) {
    return 0.0;
  }
  if ( $clade eq 'genus' ) {
    $retVal = 0.86 - ((-$phymmblScore - (1.25*$readLength - 550))**
		      (6 + 12/$readLength)) / 1.5e17 + 8/$readLength;
  } elsif ( $clade eq 'family' ) {
    $retVal = 0.97 - ((-$phymmblScore - (1.25*$readLength - 550))**
		      (6 + 12/$readLength)) / 1.5e17 + 8/$readLength;
  } elsif ( $clade eq 'order' ) {
    $retVal = 0.98 - ((-$phymmblScore - (1.25*$readLength - 550))**
		      (6 + 12/$readLength)) / 1.5e17 + 8/$readLength;
  } elsif ( $clade eq 'class' ) {
    $retVal = 0.99 - ((-$phymmblScore - (1.25*$readLength - 550))**
		      (6 + 10/$readLength)) / 1.5e17 + 8/$readLength;
  } elsif ( $clade eq 'phylum' ) {
    $retVal = 0.99 - ((-$phymmblScore - (1.25*$readLength - 550))**
		      (5.9 + 12/$readLength)) / 1.5e17 + 8/$readLength;
  }
  if ( $retVal eq 'nan' or $retVal > 1 ) {
    $retVal = 0.99999;
  } elsif ( $retVal < 0 ) {
    $retVal = 0.00001;
  }
  return $retVal;
}
sub unixEscape {
  my $arg = shift;
  $arg =~ s/\(/\\\(/g;
  $arg =~ s/\)/\\\)/g;
  $arg =~ s/\'/\\\'/g;
  $arg =~ s/\"/\\\"/g;
  $arg =~ s/ /\\ /g;
  return $arg;
}
