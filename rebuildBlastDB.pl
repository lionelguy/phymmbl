#!/usr/bin/perl

=head1 SYNOPSIS

rebuildBlastDB.pl - Rebuilds blast database

=head1 USAGE

rebuildBlastDB.pl [-d|--database <path>] [-f|--force]

=head1 INPUT

=head2  [-d|--database <path>]

Path to the phymmbl database. The subfolders in <path>/genomeData and <path>/genomeData/_userAdded will be scanned. Uses the current directory by default.

=head2 [-f|--force]

Forces the execution of the script, skips the dialog

=cut

use strict;
use Pod::Usage;
use Getopt::Long;
$| = 1;

my $datapath = '.';
my $help;
my $force;
GetOptions('d|database=s' => \$datapath,
	   'f|force'      => \$force,
	   'h|help'       => \$help,
	  );

pod2usage(-exitval => 1, -verbose => 2) if $help;
# Checks that the database mirror exists, is readable and contains files

################################################################################
# 
# Rebuild the BLAST DB.
# 
################################################################################
my $response;
unless ($force){
  print "\nDo you really  want to rebuild the BLAST database $datapath(Y/N): ";
  $response = <STDIN>;
  chomp $response;
  while ( length($response) != 1 or $response !~ /[yYnN]/ ) {
    print "Please enter Y or N: ";
    $response = <STDIN>;
    chomp $response;
  }
}
my $logDir = 'logs/setup';
system("mkdir -p $logDir");
if ( $force || $response =~ /[yY]/  ) {
  print "Rebuilding BLAST database...";
  my $res = &rebuildBlastDB($datapath);
  if ($res == 1){
    print "done.  Logs can be found at \"$logDir/rebuildBlastDB_*.txt\".\n\n";
  } else {
    print "done.  No Fasta files found in subfolders of $datapath/genomeData\n";
  }
} else {
  print "\nOkay; exiting without rebuilding BLAST database.\n\n";
}
exit 0;

# Rebuild the local BLAST database from the RefSeq FASTA files (plus any 
# genome files added by users as well).
sub rebuildBlastDB {
  my $locateBlastBinary = `which makeblastdb 2>&1`;
  die("FATAL: You must have a local copy of the BLAST software installed " . 
      "and accessible via your \"PATH\" environment variable.  Please see " . 
      "the README file for details.\n\n") 
    if ( $locateBlastBinary =~ /no\s+makeblastdb\s+in/ 
	 or $locateBlastBinary =~ /command\s+not\s+found/i );
  # Standalone BLAST is installed.
  # Concatenate all genome files and build a local BLAST DB from them.
  # First, grab a list of relative paths to all RefSeq genomic FASTA files.
  my @fastaFiles = ();
  opendir DOT, "$datapath/genomeData" 
    or die("FATAL: Can't open $datapath/genomeData for scanning.\n");
  my @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DOT;
  closedir DOT;
  foreach my $sub ( @subs ) {
    if ( -d "$datapath/genomeData/$sub" ) {
      opendir DOT, "$datapath/genomeData/$sub" 
	or die("FATAL: Can't open $datapath/genomeData/$sub for scanning.\n");
      my @files = sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;
      closedir DOT;
      foreach my $file ( @files ) {
	push @fastaFiles, "$datapath/genomeData/$sub/$file";
      }
    }
  }
  # Next, grab a list of relative paths to any user-added FASTA files.
  my $userDir = "$datapath/genomeData/_userAdded";
  if ( -e $userDir ) {
    opendir DOT, $userDir 
      or die("FATAL: Can't open $userDir for scanning.\n");
    @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DOT;
    closedir DOT;
    foreach my $sub ( @subs ) {
      if ( -d "$userDir/$sub" ) {
	opendir DOT, "$userDir/$sub" 
	  or die("FATAL: Can't open $userDir/$sub for scanning.\n");
	my @files = sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;
	closedir DOT;
	foreach my $file ( @files ) {
	  push @fastaFiles, "$userDir/$sub/$file";
	}
      }
    }
  }
  # No Fasta file found, die
  unless (@fastaFiles){
    return 0;
  }
  # Concatenate all the FASTA files together for database creation.
  my $command = 'cat ';
  my $wrapped = 0;
  my $fileIndex = -1;
  foreach my $file ( @fastaFiles ) {
    $fileIndex++;
    $command .= "$file \\\n";
    if ( ( $fileIndex + 1 ) % 10 == 0 and $fileIndex != $#fastaFiles ) {
      if ( not $wrapped ) {
	$wrapped = 1;
	$command .= "> blastDB_data.txt\n";
      } else {
	$command .= ">> blastDB_data.txt\n";
      }
      $command .= 'cat ';
    }
  }
  if ( not $wrapped ) {
    $command .= "> blastDB_data.txt\n";
  } else {
    $command .= ">> blastDB_data.txt\n";
  }
  # There's a problem with using system() for a command this long, 
  # apparently, so we use a hack.
  my $shLoc = `which sh`;
  chomp $shLoc;
  my $outFile = "command_temp.sh";
  open OUT, ">$outFile" or die("FATAL: Can't open $outFile for writing.\n");
  print OUT "#!$shLoc\n\n$command";
  close OUT;
  system("chmod 775 command_temp.sh");
  system("./command_temp.sh");
  system("rm command_temp.sh");
  # Create the local database copy.
  system("makeblastdb -in blastDB_data.txt -title phymm_BLAST_DB -out phymm_BLAST_DB -dbtype nucl > $logDir/rebuildBlastDB_out.txt 2> $logDir/rebuildBlastDB_err.txt") == 0 
    or die "Something went wrong while running makeblastdb: $?\n";
  system("mkdir -p $datapath/blastData") == 0 
    or die ("Could not mkdir $datapath/blastData") 
      unless (-e "$datapath/blastData");
  system("mv phymm_BLAST_DB.* $datapath/blastData");
  system("rm blastDB_data.txt");
  return 1;
}

