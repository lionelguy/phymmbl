#!/usr/bin/perl

=head1 SYNOPSIS

customGenomicData.pl - Perform setup and update of databases necessary to run phymmBL

=head1 USAGE

customGenomicData.pl [-d|--database <path>] [-t|--threads <int>] [<tabfile>]

=head1 INPUT

=head2 [-d|--database <path>]

The path where the phymmbl database is stored.

=head2 [-t|--threads <int>]

Number of threads to use. Minimum is 2.

=head2 [<tabfile>]

=cut

$| = 1;
use strict;
use Pod::Usage;
use Getopt::Long;
use FindBin qw/$Bin/;
use lib "$Bin/scripts";

my $datapath = '.';
my $nthreads = 2;
my $help;

GetOptions('d|database=s' => \$datapath,
	   't|threads=s'  => \$nthreads,
	   'h|help'       => \$help,
	  );

my $batchFile = shift @ARGV;
pod2usage(-exitval => 1, -verbose => 2) if $help;

my $logDir = "$datapath/logs/customGenomicData";
system("mkdir -p $logDir");
my $logFile = "$logDir/genome_addition_log.txt";
open LOG, ">>$logFile" or die("Can't open $logFile for appending.\n");
print LOG "=" x 80, "\n";
my $dateString = `date`;
print LOG "$dateString";
print LOG "=" x 80, "\n";
if ( $batchFile ) {
  # BEGIN BLOCK: MULTIPLE-ORGANISM BATCH ADDITION -- NOT INTERACTIVE
  if ( not -e $batchFile ) {
    pod2usage(-exitval => 1, -message => "Batch file $batchFile doesn't exist")
  }
  my $orgIndex = 0;
  my $fileLoc = {};
  my $singleModel = {};
  my $phylum = {};
  my $class = {};
  my $order = {};
  my $family = {};
  my $genus = {};
  my $species = {};
  my $strain = {};
  my $lineCount = 0;
  open IN, "<$batchFile" 
    or &customDie("\n   Could not open $batchFile for reading.\n\n");
  print "\n   Scanning metadata in $batchFile...";
  print LOG "\n   Scanning metadata in $batchFile...";
  while ( my $line = <IN> ) {
    $lineCount++;
    chomp $line;
    my @fields = split(/\t/, $line);
    my $numFields = $#fields + 1;
    if ( $numFields != 9 ) {
      &customDie("\n   Error in $batchFile, line $lineCount: wrong number of entries present (should be 9).  Please check your file's format against the specification given in the README.\n\n");
    } else {
      $fileLoc->{$orgIndex} = $fields[0];
      &customDie("\n   Error in $batchFile, line $lineCount: location \"$fields[0]\" doesn't exist.\n\n") if ( not -e $fields[0] );
      $singleModel->{$orgIndex} = $fields[1];
      if ( $fields[2] ne 'NO_VALUE' ) {
	$phylum->{$orgIndex} = $fields[2];
      } else {
	$phylum->{$orgIndex} = '';
      }
      if ( $fields[3] ne 'NO_VALUE' ) {
	$class->{$orgIndex} = $fields[3];
      } else {
	$class->{$orgIndex} = '';
      }
      if ( $fields[4] ne 'NO_VALUE' ) {
	$order->{$orgIndex} = $fields[4];
      } else {
	$order->{$orgIndex} = '';
      }
      if ( $fields[5] ne 'NO_VALUE' ) {
	$family->{$orgIndex} = $fields[5];
      } else {
	$family->{$orgIndex} = '';
      }
      if ( $fields[6] ne 'NO_VALUE' ) {
	$genus->{$orgIndex} = $fields[6];
      } else {
	$genus->{$orgIndex} = '';
      }
      if ( $fields[7] ne 'NO_VALUE' ) {
	$species->{$orgIndex} = $fields[7];
      } else {
	$species->{$orgIndex} = '';
      }
      if ( $fields[8] ne 'NO_VALUE' ) {
	$strain->{$orgIndex} = $fields[8];
      } else {
	$strain->{$orgIndex} = '';
      }
      $orgIndex++;
    }
  }
  close IN;
  print "done.\n\n";
  print LOG "done.\n\n";
  foreach my $i ( 0 .. $orgIndex - 1 ) {
    my $orgFailed = 0;
    my $strainText = '';
    if ( length($strain->{$i}) > 0 ) {
      $strainText = " $strain->{$i}";
    }
    my $date = `date`;
    chomp $date;
    print 
      "      [$date] Processing \"$genus->{$i} $species->{$i}$strainText\"...";
    print LOG 
      "      [$date] Processing \"$genus->{$i} $species->{$i}$strainText\"...";

    # First, check for spec-compliant FastA files at or in the target location.
    my $loc = $fileLoc->{$i};
    my @files = ();
    if ( -d $loc ) {
      # We have a directory.  Scan it and record locations of all contained 
      #FastA files.
      opendir DOT, $loc 
	or &customDie("\n\n   Can't open $loc for scanning.\n\n");
      my @locFiles = readdir DOT;
      closedir DOT;
      my $foundOne = 0;
      foreach my $file ( @locFiles ) {
	if ( $file =~ /\.fna$/i or $file =~ /\.fa$/i or $file =~ /\.fasta$/i ) {
	  $foundOne = 1;
	  my $fileCheckResult = &checkFileFormat("$loc/$file", 1);
	  if ( $fileCheckResult ) {
	    push @files, "$loc/$file";
	  } else {
	    $orgFailed = 1;
	  }
	}
      }
      if ( not $foundOne ) {
	print "\n\n   Error: No FastA files found at location \"$loc\".  ". 
	  "Skipping this organism.\n\n";
	print LOG "\n\n   Error: No FastA files found at location \"$loc\". " .
	  " Skipping this organism.\n\n";
	$orgFailed = 1;
      }
      # We're not going to allow more than 20 files for a single organism.
      # If we do, all kinds of filesystem nastiness happens.  If users have
      # some unanticipated reason for having lots of input files for a
      # single critter, the README recommends concatenation prior to running
      # this script.  If you can take the trouble to build a metadata file
      # for the whole batch, then surely you can concatenate some FastA files,
      # right?
      if ( $#files >= 20 ) {
	print "\n\n   Error: Too many FastA files (more than 20) found at " . 
	  "location \"$loc\".  Skipping this organism.\n\n";
	print LOG "\n\n   Error: Too many FastA files (more than 20) found at ".
	  "location \"$loc\".  Skipping this organism.\n\n";
	$orgFailed = 1;
      }
    } else {
      # We have a single file.  Make sure it's a FastA file that meets the 
      # spec, then add it.
      my $fileCheckResult = &checkFileFormat($loc, 1);
      push @files, $loc;
    }
    if ( not $orgFailed ) {
      # The @files array is now populated with one or more locations of FastA 
      # files to be used as input for this critter.  Check to see if one or 
      # more models were specified and process accordingly.
      # First: do we have just one file?  If so, the decision's trivial.
      my $testFileLoc = $files[1];
      my $inputFasta = '';
      if ( not $testFileLoc ) {
	# We have just one file.  Use that.
	$inputFasta = $files[0];
      } else {
	# We have multiple files.  Did we want just one model for the whole 
	# organism?
	if ( $singleModel->{$i} ) {
	  # Yes.  Aggregate all files into a single input file.
	  my $sysCmd = 'cat';
	  foreach my $file ( @files ) {
	    $sysCmd .= " $file";
	  }
	  $sysCmd .= " > ./aggregate.fasta";
	  system("$sysCmd");
	  $inputFasta = 'aggregate.fasta';
	} else {
	  # No.  Make an individual ICM for each input file.
	  $inputFasta = 'MULTIPLE';
	}
      }
      ##########################################################################
      #
      # Make a directory for the ICM and training files in a special _userAdded 
      # subdirectory of genomeData, and move all the FASTA input into that 
      # directory in the appropriate form.
      # 
      ##########################################################################
      if ( not -e "$datapath/genomeData/_userAdded" ) {
	system("mkdir $datapath/genomeData/_userAdded");
      }
      my $speciesName = $genus->{$i} . ' ' . $species->{$i};
      if ( length($strain->{$i}) > 0 ) {
	$speciesName .= ' ' . $strain->{$i};
      }
      $speciesName = &dirify($speciesName);
      my $newDirPath = "$datapath/genomeData/_userAdded/$speciesName";
      my $nextFastaID = 0;
      if ( not -e $newDirPath ) {
	system("mkdir $newDirPath");
      } else {
	# We're adding to the existing species directory.  Scan any .fna 
	# files in there to find out the current unique-prefix index so we 
	# don't muck up the existing data.
	opendir DOT, $newDirPath 
	  or &customDie("\n\n   Can't open $newDirPath for scanning; it already exists, but there's something wrong...\n\n");
	my @subs = readdir DOT;
	closedir DOT;
	foreach my $sub ( @subs ) {
	  if ( $sub =~ /\.(\d+)\.fna/ ) {
	    my $index = $1;
	    if ( $index >= $nextFastaID ) {
	      $nextFastaID = $index + 1;
	    }
	  }
	}
      }
      # Save the first FASTA ID we actually wrote to, so that we can 
      # properly update the accession map later on.
      my $firstFastaID_written = $nextFastaID;
      my $lastFastaID_written = -1;
      if ( $inputFasta ne 'MULTIPLE' ) {
	# We just have a single genome file.  Rename it to give it a new 
	# prefix that we know is going to be unique, and make sure any headers 
	# contain the assigned species name so BLAST results will correspond 
	# properly to Phymm results.
	my $oldFile = $inputFasta;
	my $tempFile = 'temp__header__rewrite__' . $speciesName . '.fna';
	my $newFile = "$newDirPath/$speciesName\.$nextFastaID\.fna";
	system("cp $oldFile $tempFile; chmod 0755 $tempFile") == 0 
	  or die "Could not copy $oldFile to $tempFile";
	open IN, "<$tempFile" 
	  or &customDie("Can't open $tempFile for reading.\n");
	open OUT, ">$newFile" 
	  or &customDie("Can't open $newFile for writing.\n");
	while ( my $line = <IN> ) {
	  if ( $line =~ /^>/ ) {
	    $line =~ s/^>/>$speciesName \| /;
	  }
	  print OUT $line;
	}
	close OUT;
	close IN;
	system("rm $tempFile");
	$lastFastaID_written = $nextFastaID;
      } else {
	# We have multiple files, and we've been asked to make a
	# single model for each input file.  Dump them all into
	# the new genome directory, renaming as we go.
	foreach my $file ( @files ) {
	  my $oldFile = $file;
	  my $tempFile = 'temp__header__rewrite__' . $speciesName . '.fna';
	  my $newFile = "$newDirPath/$speciesName\.$nextFastaID\.fna";
	  system("cp $oldFile $tempFile; chmod 0755 $tempFile" == 0 
	  or die "Could not copy $oldFile to $tempFile");
	  open IN, "<$tempFile" 
	    or &customDie("Can't open $tempFile for reading.\n");
	  open OUT, ">$newFile" or 
	    &customDie("Can't open $newFile for writing.\n");
	  while ( my $line = <IN> ) {
	    if ( $line =~ /^>/ ) {
	      $line =~ s/^>/>$speciesName \| /;
	    }
	    print OUT $line;
	  }
	  close OUT;
	  close IN;
	  system("rm $tempFile");
	  $lastFastaID_written = $nextFastaID;
	  $nextFastaID++;
	}
      }
      ##########################################################################
      # 
      # Update the local accession map and taxonomy database.
      # 
      ##########################################################################
      my $userAccFile = 
	"$datapath/taxonomyData/0_accessionMap/accessionMap_userAdded.txt";
      open OUT, ">>$userAccFile" 
	or &customDie("\n\n   Can't open $userAccFile for appending.\n\n");
      foreach my $fastaIndex ( $firstFastaID_written .. $lastFastaID_written ) {
	my $line = "$speciesName\t$speciesName\.$fastaIndex\n";
	print OUT $line;
      }
      close OUT;
      my $userDistFile = "$datapath/taxonomyData/3_parsedTaxData/" . 
	"distributionOfTaxa_userAdded.txt";
      open OUT, ">>$userDistFile" 
	or &customDie("\n\n   Can't open $userDistFile for appending.\n\n");
      my $tax = {};
      $tax->{'phylum'}  = $phylum->{$i};
      $tax->{'class'}   = $class->{$i};
      $tax->{'order'}   = $order->{$i};
      $tax->{'family'}  = $family->{$i};
      $tax->{'genus'}   = $genus->{$i};
      $tax->{'species'} = $species->{$i};
      $tax->{'strain'}  = $strain->{$i};
      foreach my $fastaIndex ( $firstFastaID_written .. $lastFastaID_written ) {
	my $accession = "$speciesName\.${fastaIndex}";
	foreach my $taxType 
	  ( 'phylum', 'class', 'order', 'family', 'genus', 'species' ) {
	  print OUT "$taxType\t$tax->{$taxType}\t$accession \($tax->{'genus'} $tax->{'species'}\)\t$speciesName\n";
	}
      }
      close OUT;
      ##########################################################################
      # 
      # Construct training files for the FASTA input as needed.
      # 
      ##########################################################################
      system("$Bin/scripts/createTrainingFiles_userAdded.pl $newDirPath 500");
      ##########################################################################
      # 
      # Build ICMs as needed.
      # 
      ##########################################################################
      system("$Bin/scripts/buildICMs_userAdded.pl $newDirPath 1");
      print "done.\n";
      print LOG "done.\n";
    } # end if ( organism didn't fail one of the sanity checks )
  } # end foreach ( organism in the batch file )
  if ( -e 'aggregate.fasta' ) {
    system("rm aggregate.fasta");
  }
  my $date = `date`;
  chomp $date;
  print "\n   Addition process complete.  Rebuilding BLAST database...";
  print LOG "\n   Addition process complete.  Rebuilding BLAST database...";
  ##############################################################################
  # 
  # Rebuild the BLAST DB.
  # 
  ##############################################################################
  &rebuildBlastDB();
  $date = `date`;
  chomp $date;
  print "done.  [$date]\n\n";
  print LOG "done.  [$date]\n\n";
} else {
  # BEGIN BLOCK: SINGLE ORGANISM ADDITION -- INTERACTIVE
  ##########################################################################
  # 
  # User interactivity prologue.  Figure out what the user wants to do, within 
  # the confines of what we're allowing.
  # 
  ##########################################################################
  my $fileLoc = '';
  my @fileLocs = ();
  # Public service announcement.
  print "\nPLEASE NOTE -- IMPORTANT: In this mode, this script can only be used to build\n";
  print "genome models for one organism at a time.  You can specify multiple fasta files\n";
  print "belonging to the same organism, and you'll be asked later whether or not you\n";
  print "want to create one model per file or create one model combining all of the\n";
  print "specified files, but either way, all models constructed in this way will\n";
  print "be associated with the same organism.\n\n";
  print "If you want to construct models for multiple organisms at once, please use\n";
  print "the batch-add function (see README for details).\n\n";
  print "------------------------------------------------------------------------\n\n";
  print LOG "[Running in interactive (single-organism entry) mode.]\n\n";
  ##########################################################################
   # 
   # Get taxonomic data.
   # 
   ##########################################################################
  my $tax = {};
  print "Please input the taxonomic data for the organism represented by your input files.\n";
  print "Every level except genus and species may be left blank (if there's no existing\n";
  print "classification for that level).\n\n";
  print "Phylum: ";
  my $response = <STDIN>;
  chomp $response;
  $tax->{'phylum'} = $response;
  print LOG "Phylum: $response\n";
  print "Class: ";
  $response = <STDIN>;
  chomp $response;
  $tax->{'class'} = $response;
  print LOG "Class: $response\n";
  print "Order: ";
  $response = <STDIN>;
  chomp $response;
  $tax->{'order'} = $response;
  print LOG "Order: $response\n";
  print "Family: ";
  $response = <STDIN>;
  chomp $response;
  $tax->{'family'} = $response;
  print LOG "Family: $response\n";
  print "Genus [required]: ";
  $response = <STDIN>;
  chomp $response;
  while ( length($response) == 0 ) {
    print "Genus [required]: ";
    $response = <STDIN>;
    chomp $response;
   }
  $tax->{'genus'} = $response;
  print LOG "Genus: $response\n";
  print "Species (not the full binomial name with the genus, but just the species label) [required]: ";
  $response = <STDIN>;
  chomp $response;
  while ( length($response) == 0 ) {
    print "Species (not the full binomial name with the genus, but just the species label) [required]: ";
    $response = <STDIN>;
    chomp $response;
   }
  $tax->{'species'} = $response;
  print LOG "Species: $response\n";
  print "Strain: ";
  $response = <STDIN>;
  chomp $response;
  $tax->{'strain'} = $response;
  print LOG "Strain: $response\n";
  print "\n";
  my $displayBinomialAndStrain = "$tax->{'genus'} $tax->{'species'}";
  if ( length($tax->{'strain'}) > 0 ) {
    $displayBinomialAndStrain .= " $tax->{'strain'}";
  }
  ##########################################################################
  # 
  # Get input file information.
  # 
  ##########################################################################
  print "Adding custom genome for \"$displayBinomialAndStrain.\"  Genome data will be built from fasta files provided by you.\n\n";
  print "Have you got one or more files? [Enter S for a single file (default), M for more]: ";
  print LOG "Adding custom genome data for \"$displayBinomialAndStrain.\"\n\n";
  $response = <STDIN>;
  chomp $response;
  if ( length($response) > 1 
       or ( length($response) == 1 and $response !~ /[sSmM]/ ) ) {
    print "\n   Please select from among the options given.\n\n";
    while ( length($response) > 1 
	    or ( length($response) == 1 and $response !~ /[sSmM]/ ) ) {
      print "[Enter S for a single file (default), M for more]: ";
      $response = <STDIN>;
      chomp $response;
    }
  }
  if ( length($response) == 0 or $response =~ /[sS]/ ) {
    # Just one input file.  Ask for its location and scan it to make sure 
    # it's a valid fasta file.
    my $foundCorrectFileType = 0;
    while ( $foundCorrectFileType != 1 ) {
      print "\nPlease enter the location of the fasta file you wish to add to the database: ";
      $fileLoc = <STDIN>;
      chomp $fileLoc;
      if ( not -e $fileLoc ) {
	print "\n   The file you specified doesn't appear to exist.\n";
      } elsif ( not -f $fileLoc or -B $fileLoc ) {
	print "\n   The location you entered isn't a regular file.  Please provide a fasta file.\n";
      } else {
	print "\n   Checking file format...";
	$foundCorrectFileType = &checkFileFormat($fileLoc);
	print "done.  File complies with standard nucleotide fasta spec.\n\n";
      }
    }
  } elsif ( $response =~ /[mM]/ ) {
    # The user has multiple fasta input files.
    # 
    # Figure out whether they just want to provide a directory location 
    # which will then be scanned for fasta files,
    # or they want to enter file locations by hand one at a time.
    print "\nYou can specify a directory to scan for fasta files contained within it (all files\n";
    print "with the extensions \".fasta\", \".fa\" and \".fna\" [not case-sensitive] will be used;\n";
    print "all others in the specified directory will be ignored), or you can specify the files one at a time.\n\n";
    print "Please select D for a directory scan (default) or M for manual entry of fasta file locations, one at a time: ";
    $response = <STDIN>;
    chomp $response;
    if ( length($response) > 1 
	 or ( length($response) == 1 and $response !~ /[dDmM]/ ) ) {
      print "\n   Please select from among the options given.\n\n";
      while ( length($response) > 1 
	      or ( length($response) == 1 and $response !~ /[dDmM]/ ) ) {
	print 
	  "[Enter D for directory scan (default), M for manual file entry]: ";   	    $response = <STDIN>;
	chomp $response;
      }
    }
    if ( length($response) == 0 or $response =~ /[dD]/ ) {
      # User wants to specify a directory to scan.  Ask for its location, 
      # check for the presence of at least one fasta file,
      # and scan all contained fasta files for correct formatting.
      my $foundGoodDir = 0;
      while ( not $foundGoodDir ) {
	print "\nPlease enter the location of the directory to scan: ";
	my $dirLoc = <STDIN>;
	chomp $dirLoc;
	if ( not -e $dirLoc ) {
	  print "\n   The directory you specified doesn't appear to exist.\n";
	} elsif ( not -d $dirLoc ) {
	  print "\n   The location you specified isn't a directory.\n";
	} else {
	  print "\n   Directory located.  Scanning for fasta files...\n";
	  opendir DOT, $dirLoc or &customDie("\nCan't open $dirLoc for scanning.  Aborting.\n\n");
	  my @files = readdir DOT;
	  closedir DOT;
	  my $foundFile = 0;
	  foreach my $file ( @files ) {
	    if ( $file =~ /\.fna$/i 
		 or $file =~ /\.fa$/i or $file =~ /\.fasta$/i ) {
	      $foundFile = 1;
	      print "\n      Found file $file\.  Checking file format...";
	      my $fileCheckResult = &checkFileFormat("$dirLoc/$file");
	      print 
		"done.  File complies with standard nucleotide fasta spec.\n"; 
	      push @fileLocs, "$dirLoc/$file";
	    }
	  }
	  if ( not $foundFile ) {
	    &customDie("\n   ...done.  No fasta files (extensions .fna, .fa, or .fasta) were found in the specified directory.  Aborting.\n\n");
	  } else {
	    $foundGoodDir = 1;
	    my $fileCount = $#fileLocs + 1;
	    my $plural = 's';
	    if ( $fileCount == 1 ) {
	      $plural = '';
	    }
	    print "\n   ...done.  $fileCount fasta file$plural found and verified as valid.\n\n";
	  }
	}
      }
    } elsif ( $response =~ /[mM]/ ) {
      # User would like to specify multiple fasta files, one at a time.  
      # Ask for their locations, and scan each for correct formatting.
      my $done = 0;
      while ( not $done ) {
	print "\nPlease enter the name of a file to use, or enter a blank line when you're done: ";
	$response = <STDIN>;
	chomp $response;
	if ( length($response) == 0 ) {
	  $done = 1;
	} else {
	  my $nextFile = $response;
	  if ( &alreadyEntered($nextFile, \@fileLocs) ) {
	    print "\n   You already entered that file.\n";
	  } elsif ( not -e $nextFile ) {
	    print "\n   The file you specified doesn't appear to exist.\n";
	  } elsif ( not -f $nextFile or -B $nextFile ) {
	    print "\n   The location you entered isn't a regular file.  Please provide a fasta file.\n";
	  } else {
	    print "\n   Checking file format...";
	    my $fileCheckResult = &checkFileFormat($nextFile);
	    print "done.  File complies with standard nucleotide fasta spec.\n";
	    push @fileLocs, $nextFile;
	  }
	}
      }
      my $fileCount = $#fileLocs + 1;
      my $plural = 's';
      if ( $fileCount == 0 ) {
	&customDie("\n   You didn't enter any files.  Aborting.\n\n");
      } elsif ( $fileCount == 1 ) {
	$plural = '';
      }
      print "\nDone.  $fileCount file$plural entered and verified as valid.\n\n";  
    }
  }
  ##########################################################################
  # 
  # We've now got either a single file to process or a list of multiple files
  # to work on.  All files have been verified as valid fasta files.
  # 
  # If we've got multiple files, find out whether the user wants to create
  # a single genome model per file, or to combine all the files into a single
  # model.
  # 
  ##########################################################################
  my $combineMultipleFiles = 1;
  if ( $fileLoc eq '' ) {
    # If $fileLoc never got set, we have a list of files instead.  Is there more
    # than one file in the list?  (User could've selected a directory with just
    # one fasta file in it; we need to check.)
    my $fileCount = $#fileLocs + 1;
    if ( $fileCount > 1 ) {
      # We've got multiple files in the list.  Poll the user on how to proceed.
      print "OK: you've entered multiple files.  We can proceed in one of two ways.\n\n";
      print "We can construct one genome model per file (recommended if you have, say,\n";
      print "one file per chromosome or plasmid), or we can combine the data in all\n";
      print "the files into a single genome model (recommended if you have, for example,\n";
      print "files containing multiple chromosomal contigs, sequenced and assembled\n";
      print "from the same organism.)\n\n";
      print "--- NOTE AGAIN that regardless of which option you choose, all models that are\n";
      print "built will be associated with the same species.  If you want to construct models\n";
      print "for multiple species, you need to run this script multiple times. ---\n\n";
      print "How would you like to proceed?\n\n";
      print "[Enter S for a single combined model (default), or M for multiple models]: ";
      $response = <STDIN>;
      chomp $response;
      while ( length($response) > 1 
	      or ( length($response) == 1 and $response !~ /[sSmM]/ ) ) {
	print "[Enter S for a single combined model (default), or M for multiple models]: ";
	$response = <STDIN>;
	chomp $response;
      }
      if ( $response =~ /m/i ) {
	# One model per file.
	$combineMultipleFiles = 0;
	print LOG "Making $fileCount models from $fileCount input files:\n\n";
	print LOG join("\n", @fileLocs) . "\n";
      } else {
	# Combine files into a single model.  
	# This is the default setting for $combineMultipleFiles.
	print LOG "Making a single genome model from $fileCount input files:\n\n";
	print LOG join("\n", @fileLocs) . "\n";
      }
      print "\n";
    }
  } else {
    # $fileLoc got set: we're only processing one single input file.
    print LOG "Making a genome model from the following input file:\n\n";
    print LOG "$fileLoc\n\n";
  }
  ##############################################################################
  # 
  # Prompt the user to regenerate the BLAST database at the end of the 
  # automated phase following this one.  If they're going to be running this 
  # script multiple times in one sitting, they may not want to do this bit 
  # yet, so we've provided a separate script to do that manually when they're 
  # ready.
  # 
  #############################################################################
  my $regenBlastPlural = 's';
  my $regenBlastVerb = 'have';
  if ( $fileLoc != '' or $#fileLocs < 1 ) {
    # Just one input file.
    $regenBlastPlural = '';
    $regenBlastVerb = 'has';
  }
  print "Once your input file${regenBlastPlural} $regenBlastVerb been processed, the local BLAST database will be automatically\n";
  print "rebuilt to reflect the new sequence data.  If for any reason you don't want this to happen\n";
  print "automatically, you can use the \"rebuildBlastDB.pl\" script to do so manually;\n";
  print "this would be useful if you intend to run this script multiple times to add multiple\n";
  print "organisms to the database, and don't want to wait for the BLAST DB to be rebuilt each time.\n\n";
  print "If you select manual regeneration, please be cautioned that if the DB is not regenerated\n";
  print "when you've finished - either automatically on the last run of this script, or manually\n";
  print "using \"rebuildBlastDB.pl\" - your PhymmBL results will be *wrong*, because Phymm and\n";
  print "BLAST will be operating with different datasets.\n\n";
  print "Given all that, would you like to automatically rebuild the BLAST DB when this script has\n";
  print "finished (A, default) or rebuild the BLAST DB by hand later on (H)? : ";
  $response = <STDIN>;
  chomp $response;
  while ( length($response) > 1 or ( length($response) == 1 and $response !~ /[aAhH]/ ) ) {
    print "\nPlease enter (A) for automatic regeneration when this script has completed,\n";
    print "or (H) for regeneration by hand using \"rebuildBlastDB.pl\" when you've finished\n";
    print "entering all your genome data: ";
    $response = <STDIN>;
    chomp $response;
  }
  my $rebuildBlastDB = 1;
  if ( $response =~ /[hH]/ ) {
    # Waive the auto-regeneration.
    $rebuildBlastDB = 0;
    print LOG "User has declined to rebuild the BLAST database following addition of genomic data.\n\n";
  } elsif ( $response =~ /[aA]/ or length($response) == 0 ) {
    # Default.  $rebuildBlastDB has already been set to 1.
    print LOG "User has wisely accepted the default practice of rebuilding the BLAST database following addition of genomic data.\n\n";
  }
  ##############################################################################
  # 
  # Make a directory for the ICM and training files in a special _userAdded 
  # subdirectory of genomeData, and move all the FASTA input into that 
  # directory in the appropriate form.
  #
  ##############################################################################
  print "\n\n   Thank you.  The user-input phase has now ended.  Feel free to go get some coffee or something. :)\n\n\n";
  print "Configuring directory structure...";
  print LOG "Configuring directory structure...";
  if ( not -e "$datapath/genomeData/_userAdded" ) {
    system("mkdir $datapath/genomeData/_userAdded");
  }
  my $speciesName = $tax->{'genus'} . ' ' . $tax->{'species'};
  if ( length($tax->{'strain'}) > 0 ) {
    $speciesName .= ' ' . $tax->{'strain'};
  }
  $speciesName = &dirify($speciesName);
  my $newDirPath = "$datapath/genomeData/_userAdded/$speciesName";
  my $nextFastaID = 0;
  if ( not -e $newDirPath ) {
    system("mkdir $newDirPath");
  } else {
    # We're adding to the existing species directory.  Scan any .fna files 
    # in there to find out the current unique-prefix index so we don't muck 
    # up the existing data.
    opendir DOT, $newDirPath or &customDie("\n\n   Can't open $newDirPath for scanning; it already exists, but there's something wrong...\n\n");
    my @subs = readdir DOT;
    closedir DOT;
    foreach my $sub ( @subs ) {
      if ( $sub =~ /\.(\d+)\.fna/ ) {
	my $index = $1;
	if ( $index >= $nextFastaID ) {
	  $nextFastaID = $index + 1;
	}
      }
    }
  }
  # Save the first FASTA ID we actually wrote to, so that we can properly 
  # update the accession map later on.
  my $firstFastaID_written = $nextFastaID;
  print "done.\n";
  print LOG "done.\n";
  my $plural = 's';
  if ( $fileLoc ne '' or $#fileLocs == 0 ) {
    $plural = '';
  }
  my $verb = "Copying";
  if ( $combineMultipleFiles ) {
    $verb = "Concatenating";
  }
  print "$verb fasta input file${plural}...";
  print LOG "$verb fasta input file${plural}...";
  my $lastFastaID_written = -1;
  if ( $fileLoc ne '' ) {
    # We just have a single genome file.  Rename it to give it a new prefix 
    # that we know is going to be unique, and make sure any headers contain 
    # the assigned species name so BLAST results will correspond properly to
    # Phymm results.
    my $oldFile = $fileLoc;
    my $tempFile = 'temp__header__rewrite__' . $speciesName . '.fna';
    my $newFile = "$newDirPath/$speciesName\.$nextFastaID\.fna";
    system("cp $oldFile $tempFile; chmod 0755 $tempFile") == 0 
      or die "Could not copy $oldFile to $tempFile";
    open IN, "<$tempFile" or &customDie("Can't open $tempFile for reading.\n");
    open OUT, ">$newFile" or &customDie("Can't open $newFile for writing.\n");
    while ( my $line = <IN> ) {
      if ( $line =~ /^>/ ) {
	$line =~ s/^>/>$speciesName \| /;
      }
      print OUT $line;
    }
    close OUT;
    close IN;
    system("rm $tempFile");
    $lastFastaID_written = $nextFastaID;
  } elsif ( $#fileLocs == 0 ) {
    # Again, we just have a single genome file, but this time, it's in the 
    # fileLocs array.  Rename it to give it a new prefix that we know is 
    # going to be unique.
    my $oldFile = $fileLocs[0];
    my $tempFile = 'temp__header__rewrite__' . $speciesName . '.fna';
    my $newFile = "$newDirPath/$speciesName\.$nextFastaID\.fna";
    system("cp $oldFile $tempFile; chmod 0755 $tempFile") == 0 
      or die "Could not copy $oldFile to $tempFile";
    open IN, "<$tempFile" or &customDie("Can't open $tempFile for reading.\n");
    open OUT, ">$newFile" or &customDie("Can't open $newFile for writing.\n");
    while ( my $line = <IN> ) {
      if ( $line =~ /^>/ ) {
	$line =~ s/^>/>$speciesName \| /;
      }
      print OUT $line;
    }
    close OUT;
    close IN;
    system("rm $tempFile");
    $lastFastaID_written = $nextFastaID;
  } else {
    # We have multiple files.
    if ( $combineMultipleFiles ) {
      # We've been asked to combine them into a single model.  Start by 
      # concatenating them and dumping the result in the new genome directory.
      my $sysCmd = 'cat ';
      foreach my $file ( @fileLocs ) {
	$sysCmd .= "$file ";
      }
      my $newFile = "$newDirPath/$speciesName\.$nextFastaID\.fna";
      my $tempFile = 'temp__header__rewrite__' . $speciesName . '.fna';
      $sysCmd .= "> $tempFile";
      system($sysCmd);
      open IN, "<$tempFile" or &customDie("Can't open $tempFile for reading.\n");
      open OUT, ">$newFile" or &customDie("Can't open $newFile for writing.\n");
      while ( my $line = <IN> ) {
	if ( $line =~ /^>/ ) {
	  $line =~ s/^>/>$speciesName \| /;
	}
	print OUT $line;
      }
      close OUT;
      close IN;
      system("rm $tempFile");
      $lastFastaID_written = $nextFastaID;
    } else {
      # We've been asked to make a single model for each input file.  Dump 
      # them all into the new genome directory, renaming as we go.
      foreach my $file ( @fileLocs ) {
	my $oldFile = $file;
	my $tempFile = 'temp__header__rewrite__' . $speciesName . '.fna';
	my $newFile = "$newDirPath/$speciesName\.$nextFastaID\.fna";
	system("cp $oldFile $tempFile; chmod 0755 $tempFile") == 0 
	  or die "Could not copy $oldFile to $tempFile";
	open IN, "<$tempFile" 
	  or &customDie("Can't open $tempFile for reading.\n");
	open OUT, ">$newFile" 
	  or &customDie("Can't open $newFile for writing.\n");
	while ( my $line = <IN> ) {
	  if ( $line =~ /^>/ ) {
	    $line =~ s/^>/>$speciesName \| /;
	  }
	  print OUT $line;
	}
	close OUT;
	close IN;
	system("rm $tempFile");
	$lastFastaID_written = $nextFastaID;
	$nextFastaID++;
      }
    }
  }
  print "done.\n\n";
  print LOG "done.\n\n";
  #############################################################################
  # 
  # Update the local accession map and taxonomy database.
  # 
  #############################################################################
  print "Updating taxonomic data store...";
  print LOG "Updating taxonomic data store...";
  my $userAccFile = "$datapath/taxonomyData/0_accessionMap/" . 
    "accessionMap_userAdded.txt";
  open OUT, ">>$userAccFile" 
    or &customDie("\n\n   Can't open $userAccFile for appending.\n\n");
  foreach my $fastaIndex ( $firstFastaID_written .. $lastFastaID_written ) {
    my $line = "$speciesName\t$speciesName\.$fastaIndex\n";
    print OUT $line;
  }
  close OUT;
  my $userDistFile = "$datapath/taxonomyData/3_parsedTaxData/" . 
    "distributionOfTaxa_userAdded.txt";
  open OUT, ">>$userDistFile" 
    or &customDie("\n\n   Can't open $userDistFile for appending.\n\n");
  foreach my $fastaIndex ( $firstFastaID_written .. $lastFastaID_written ) {
    my $accession = "$speciesName\.${fastaIndex}";
    foreach my $taxType 
      ( 'phylum', 'class', 'order', 'family', 'genus', 'species' ) {
      print OUT "$taxType\t$tax->{$taxType}\t$accession \($tax->{'genus'} $tax->{'species'}\)\t$speciesName\n";
    }
  }
  close OUT;
  print "done.\n\n";
  print LOG "done.\n\n";
  #############################################################################
  # 
  # Construct training files for the FASTA input as needed.
  # 
  #############################################################################
  print "Constructing genomic training files for IMMs as needed...";
  print LOG "Constructing genomic training files for IMMs as needed...";
  system("$Bin/scripts/createTrainingFiles_userAdded.pl $newDirPath 500");
  print "done.\n\n";
  print LOG "done.\n\n";
  ##############################################################################
  # 
  # Build ICMs as needed.
  # 
  ##############################################################################
  print "Building ICMs as needed...";
  print LOG "Building ICMs as needed...";
  system("$Bin/scripts/buildICMs_userAdded.pl $newDirPath");
  print "done.\n\n";
  print LOG "done.\n\n";
  #############################################################################
  #
  # Rebuild the BLAST DB unless the user asked us not to.
  # 
  ##############################################################################
  if ( $rebuildBlastDB ) {
    print "Rebuilding BLAST database...";
    print LOG "Rebuilding BLAST database (toplevel):\n\n";
    &rebuildBlastDB();
    print "done.\n\n";
    print LOG "\n...done rebuilding BLAST database (toplevel).\n\n";
  }
} # END SWITCH ON BATCH VS. SINGLE-ORG ADDITION(S)
my $signoff = &generateSignoffString();
print LOG $signoff;
close LOG;


##########################################################################
#                                                                        #
#                             SUBROUTINES                                #
#                                                                        #
##########################################################################
# Check the specified input file to make sure it's a valid [multi]fasta file.  Return 1 for success, 2 for disallowed
# characters in non-comment lines.
sub checkFileFormat {
  my $filePath = shift;
  my $skipCode = shift;
  if ( $skipCode ) {
    $skipCode = 1;
  } else {
    $skipCode = 0;
  }
  my $formatOK = 1;
  open IN, "<$filePath" 
    or &customDie("\nCan't open $filePath for format checking.\n");
  my $lineCount = 0;
  while ( my $line = <IN> ) {
    $lineCount++;
    chomp $line;
    if ( $line =~ /^>/ or $line =~ /^\;/ ) {
      # Comment line.  These aren't the droids you're looking for.
    } elsif ( $line =~ /\S\s\S/ ) {
      # We'll accept leading and trailing spaces just to be helpful, 
      # but the sequence data itself can't contain spaces.
      &customDie("\n\n   There's a space character that isn't supposed to be where it is in file $filePath (line $lineCount).  Aborting.\n\n");
    } elsif ( $line =~ /(\S+)/ ) {
      # We've got the data from the current data line; check that it's 
      # limited to the proper nucleotide codes.
      my $lineData = $1;
      if ( $lineData !~ /^[aAcCgGtTuUrRyYkKmMsSwWbBdDhHvVnNxX]+$/ ) {
	$lineData =~ /([^aAcCgGtTuUrRyYkKmMsSwWbBdDhHvVnNxX])/;
	my $badChar = $1;
	if ( not $skipCode ) {
	  &customDie("\n\n   There's a disallowed nucleotide symbol (\"$badChar\") on data line $lineCount in file $filePath\.\n"
		     . "   Allowable symbols are [ACGTURYKMSWBDHVNX].  If you're using a protein fasta file, that won't work properly.\n"
		     . "   Please submit only nucleotide fasta files for processing.  Aborting.\n\n");
	} else {
	  print("\n\n   There's a disallowed nucleotide symbol (\"$badChar\") on data line $lineCount in file $filePath\.\n"
		.     "   Allowable symbols are [ACGTURYKMSWBDHVNX].  If you're using a protein fasta file, that won't work properly.\n"
		. "   Please submit only nucleotide fasta files for processing.  Skipping this organism and continuing with the next.\n\n");
	  $formatOK = 0;
	}
      }
    }
  }
  close IN;
  return $formatOK;
}

# Check to see if the file argument is already contained in the array argument. 
# If it is, return 1, otherwise return 0.
sub alreadyEntered {
  my $file = shift;
  my $arrayRef = shift;
  my @arrayArg = @$arrayRef;
  my $maxIndex = $#arrayArg;
  if ( $maxIndex == -1 ) {
    return 0;
  } else {
    for ( my $i = 0; $i <= $maxIndex; $i++ ) {
      if ( $file eq $arrayArg[$i] ) {
	return 1;
      }
    }
  }
  return 0;
}

# Transform an arbitrary ASCII string into a standardized directory name.
sub dirify {
  my $result = shift;
  $result =~ s/\_/UNDERSCORE/g;
  $result =~ s/\+/PLUS/g;
  $result =~ s/\s+/\_/g;
  $result =~ s/\//SLASH/g;
  $result =~ s/\(/LPAREN/g;
  $result =~ s/\)/RPAREN/g;
  $result =~ s/\'/SINGLEQUOTE/g;
  $result =~ s/\"/DOUBLEQUOTE/g;
  $result =~ s/\:/COLONCOLON/g;
  $result =~ s/\;/SEMICOLON/g;
  return $result;
}
# Rebuild the BLAST DB.
sub rebuildBlastDB {
  my $locateBlastBinary = `which makeblastdb 2>&1`;
  if ( $locateBlastBinary =~ /no\s+makeblastdb\s+in/ 
       or $locateBlastBinary =~ /command\s+not\s+found/i ) {
    &customDie("FATAL: You must have a local copy of the BLAST software installed and accessible via your \"PATH\" environment variable.  Please see the README file for details.\n\n");
  } else {
    # Standalone BLAST is installed.  Concatenate all genome files and build a local BLAST DB from them.
    print LOG "Generating local BLAST database from RefSeq and user-added genome data...\n\n";
    # First, grab a list of relative paths to all the RefSeq genomic FASTA files
    my @fastaFiles = ();
    opendir DOT, "$datapath/genomeData" 
      or &customDie("FATAL: Can't open $datapath/genomeData for scanning.\n");
    my @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DOT;
    closedir DOT;
    foreach my $sub ( @subs ) {
      if ( -d "$datapath/genomeData/$sub" ) {
	opendir DOT, "$datapath/genomeData/$sub" 
	  or &customDie("FATAL: Can't open $datapath/genomeData/$sub" . 
			" for scanning.\n");
	my @files = sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;
	closedir DOT;
	foreach my $file ( @files ) {
	  push @fastaFiles, "$datapath/genomeData/$sub/$file";
	}
      }
    }
    # Next, grab a list of relative paths to any user-added FASTA files.
    my $userDir = "$datapath/genomeData/_userAdded";
    if ( -e $userDir ) {
      opendir DOT, $userDir 
	or &customDie("FATAL: Can't open $userDir for scanning.\n");
      @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DOT;
      closedir DOT;
      foreach my $sub ( @subs ) {
	if ( -d "$userDir/$sub" ) {
	  opendir DOT, "$userDir/$sub" 
	    or &customDie("FATAL: Can't open $userDir/$sub for scanning.\n");
	  my @files = sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;
	  closedir DOT;
	  foreach my $file ( @files ) {
	    push @fastaFiles, "$userDir/$sub/$file";
	  }
	}
      }
    }
    # Concatenate all the FASTA files together for database creation.
    my $command = 'cat ';
    my $argCount = 0;
    my $wrapped = 0;
    my $fileIndex = -1;
    foreach my $file ( @fastaFiles ) {
      $fileIndex++;
      $command .= "$file \\\n";
      $argCount++;
      if ( $argCount % 10 == 0 and $fileIndex != $#fastaFiles ) {
	if ( not $wrapped ) {
	  $wrapped = 1;
	  $command .= "> blastDB_data.txt\n";
	} else {
	  $command .= ">> blastDB_data.txt\n";
	}
	$command .= 'cat ';
      }
    }
    if ( not $wrapped ) {
      $command .= "> blastDB_data.txt\n";
    } else {
      $command .= ">> blastDB_data.txt\n";
    }
    # There's a problem with using system() for a command this long, 
    # apparently, so we use a hack.
    my $shLoc = `which sh`;
    chomp $shLoc;
    my $outFile = "command_temp.sh";
    open OUT, ">$outFile" 
      or &customDie("FATAL: Can't open $outFile for writing.\n");
    print OUT "#!$shLoc\n\n$command";
    close OUT;
    system("chmod 775 command_temp.sh");
    system("./command_temp.sh");
    system("rm command_temp.sh");
    # Create the local database copy.
    close LOG;
    system("makeblastdb -in blastDB_data.txt -title phymm_BLAST_DB -out phymm_BLAST_DB -dbtype nucl >> $logFile 2> $logFile");
    system("mv phymm_BLAST_DB.* $datapath/blastData");
    system("rm blastDB_data.txt");

    print "done.\n\n";
    open LOG, ">>$logFile" or &customDie("FATAL: $0\::rebuildBlastDB(): Can't open $logFile for appending.\n");
    print LOG "\ndone.\n\n";
  } # end if ( local copy of BLAST binary is installed )
}

# Generate a closing block to end the current log entry cleanly.
sub generateSignoffString {
  my $result = "================================================================================\n";
  my $localDateString = `date`;
  chomp $localDateString;
  $result .= "Run completed $dateString\n";
  $result .= "================================================================================\n";
  return $result;
}

# Exit cleanly, wrapping the current log entry before dying.
sub customDie {
  my $deathString = shift;
  print LOG $deathString;
  my $signoff = &generateSignoffString();
  print LOG $signoff;
  close LOG;
  die($deathString);
}
