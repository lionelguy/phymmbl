#!/usr/bin/perl
use strict;
$| = 1;
my ($outdir, $domain) = @ARGV;

die "$0: FATAL: outdir and/or domain not provided\n" 
  unless ($outdir && $domain);
die "$0: FATAL: domain can only be one of archaea, bacteria\n"
  unless ($domain eq "bacteria" || $domain eq "archaea");
system("mkdir -p $outdir/2_flatTaxFiles") == 0 
  or die "$0: FATAL: Cannot mkdir $outdir/2_flatTaxFiles: $?\n" 
  unless (-d "$outdir/2_flatTaxFiles");

my $currentHierarchy = {};
my @taxStack = ();
my $lastEntityType = '';
my $lastEntityName = '';

my $inFile = "$outdir/1_fromNCBI/${domain}_taxonomy_stripped.html";
open IN, "<$inFile" or die("$0: FATAL: Can't open $inFile for reading.\n");
my $outFile = "$outdir/2_flatTaxFiles/flat_${domain}_taxonomy.txt";
open OUT, ">$outFile" or die("$0: FATAL: Can't open $outFile for writing.\n");
print OUT "---\n";
while ( my $line = <IN> ) {
  if ( $line =~ /<li.*?><a\s+title\=\"(.+?)\">(.+?)<\/a>/i ) {
    $lastEntityType = $1;
    $lastEntityName = $2;
    if ( $lastEntityType =~ /\"/ or $lastEntityName =~ /\"/ ) {
      die("$0: FATAL: Parse error: $lastEntityType $lastEntityName\n");
    }
    print OUT "NAME: \"$lastEntityName\"\n";
    print OUT "TYPE: \"$lastEntityType\"\n";
    foreach my $entityType ( @taxStack ) {
      print OUT "$entityType\: \"$currentHierarchy->{$entityType}\"\n";
    }
    print OUT "$lastEntityType: \"$lastEntityName\"\n";
    print OUT "---\n";
  } elsif ( $line =~ /<ul>/i and $lastEntityType ne '' ) {
    $currentHierarchy->{$lastEntityType} = $lastEntityName;
    push @taxStack, $lastEntityType;
  } elsif ( $line =~ /<\/ul>/i ) {
    my $removeEntityType = pop @taxStack;
    delete $currentHierarchy->{$removeEntityType};
  }
}
close OUT;
close IN;
exit 0;
