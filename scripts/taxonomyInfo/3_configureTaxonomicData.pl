#!/usr/bin/perl

$| = 1;
use strict;

my ($genomeDataDir, $outdir) = @ARGV; 

my @taxa;
my $seen = {};
my $tax = {};

system("mkdir -p $outdir/3_parsedTaxData") == 0 
  or die "$0: FATAL: Cannot mkdir $outdir/3_parsedTaxData: $?\n" 
  unless (-d "$outdir/3_parsedTaxData");

# Load bacterial data.
my $taxFile = "$outdir/2_flatTaxFiles/flat_bacteria_taxonomy.txt";
open IN, "<$taxFile" or die("$0: FATAL: Can't open $taxFile for reading.\n");
my $data = {};
my $object = {};
my $first = 1;
my $fieldID = 0;
while ( my $line = <IN> ) {
  if ( $line =~ /^\-\-\-/ ) {
    if ( $first == 1 ) {
      $first = 0;
    } else {
      # Filter out mid-level taxa.
      if ( $object->{'TYPE'} ne 'species' and $object->{'TYPE'} ne 'no rank' 
	   and $object->{'TYPE'} ne 'subspecies' ) {
	$object = {};
	$fieldID = 0;
      } else {
	foreach my $id ( sort {$a <=> $b} keys %{$object} ) {
	  if ( $id ne 'NAME' and $id ne 'TYPE' ) {
	    $data->{$object->{'NAME'}}->{$id}->{'label'} = 
	      $object->{$id}->{'label'};
	    $data->{$object->{'NAME'}}->{$id}->{'value'} = 
	      $object->{$id}->{'value'};
	    my $taxon = $object->{$id}->{'label'};
	    if ( not $seen->{$taxon} ) {
	      push @taxa, $taxon;
	      $seen->{$taxon} =1;
	    }
	  }
	}
	$object = {};
	$fieldID = 0;
      }
    }
  } elsif ( $line =~ /^NAME\:\s+\"(.+)\"/ ) {
    $object->{'NAME'} = $1;
  } elsif ( $line =~ /^TYPE\:\s+\"(.+)\"/ ) {
    $object->{'TYPE'} = $1;
  } elsif ( $line =~ /^(.+?)\:\s+\"(.*)\"/ ) {
    my $label = $1;
    my $value = $2;
    $object->{$fieldID}->{'label'} = $label;
    $object->{$fieldID}->{'value'} = $value;
    $fieldID++;
  } else {
    die("$0: FATAL: Unexpected line in taxonomy file \"$taxFile\": $line");
  }
}
close IN;

# Load archaeal data.
$taxFile = "$outdir/2_flatTaxFiles/flat_archaea_taxonomy.txt";
open IN, "<$taxFile" or die("$0: FATAL: Can't open $taxFile for reading.\n");
$object = {};
$first = 1;
$fieldID = 0;
while ( my $line = <IN> ) {
  if ( $line =~ /^\-\-\-/ ) {
    if ( $first == 1 ) {
      $first = 0;
    } else {
      # Filter out mid-level taxa.
      if ( $object->{'TYPE'} ne 'species' and $object->{'TYPE'} ne 'no rank' and $object->{'TYPE'} ne 'subspecies' ) {
	$object = {};
	$fieldID = 0;
      } else {
	foreach my $id ( sort {$a <=> $b} keys %{$object} ) {
	  if ( $id ne 'NAME' and $id ne 'TYPE' ) {
	    $data->{$object->{'NAME'}}->{$id}->{'label'} = 
	      $object->{$id}->{'label'};
	    $data->{$object->{'NAME'}}->{$id}->{'value'} = 
	      $object->{$id}->{'value'};
	  }
	}
	$object = {};
	$fieldID = 0;
      }
    }
  } elsif ( $line =~ /^NAME\:\s+\"(.+)\"/ ) {
    $object->{'NAME'} = $1;
  } elsif ( $line =~ /^TYPE\:\s+\"(.+)\"/ ) {
    $object->{'TYPE'} = $1;
  } elsif ( $line =~ /^(.+?)\:\s+\"(.*)\"/ ) {
    my $label = $1;
    my $value = $2;
    $object->{$fieldID}->{'label'} = $label;
    $object->{$fieldID}->{'value'} = $value;
    $fieldID++;
  } else {
    die("$0: FATAL: Unexpected line in taxonomy file \"$taxFile\": $line");
  }
}
close IN;

#my $genomeDataDir = 'genomeData';
opendir DOT, $genomeDataDir 
  or die("$0: FATAL: Can't open genomeDataDir for scanning.\n");
my @subdirs = readdir DOT;
closedir DOT;
system("rm $outdir/3_parsedTaxData/distributionOfTaxa.txt") 
  if ( -e "$outdir/3_parsedTaxData/distributionOfTaxa.txt" );

foreach my $subdir ( @subdirs ) {
  if ( -d "$genomeDataDir/$subdir" and $subdir !~ /^(\.|_userAdded)/ ) {
    my $orgName = $subdir;
    my $dirPrefix = $subdir;
    $orgName =~ s/\_/ /g;
    $orgName =~ s/UNDERSCORE/\_/g;
    $orgName =~ s/PLUS/\+/g;
    $orgName =~ s/SLASH/\//g;
    $orgName =~ s/LPAREN/\(/g;
    $orgName =~ s/RPAREN/\)/g;
    $orgName =~ s/SINGLEQUOTE/\'/g;
    $orgName =~ s/DOUBLEQUOTE/\"/g;
    $orgName =~ s/COLONCOLON/\:/g;
    $orgName =~ s/SEMICOLON/\;/g;
    if ( $orgName !~ /cicadellinicola/ 
	 and $orgName !~ /aphidicola/ 
	 and $orgName !~ /Ruthia\s+magnifica/ 
	 and $orgName !~ /biflexa\s+serovar\s+Patoc\s+strain/ 
	 and $orgName !~ /Candidatus\s+Hamiltonella\s+defensa\s+5AT/ 
	 and $orgName !~ /Wigglesworthia\s+glossinidia\s+endosymbiont\s+of\s+Glossina\s+morsitans/ 
	 and $orgName !~ /^Corynebacterium\s+diphtheriae\s+C7/ ) {
      $orgName =~ s/\s+\(.*$//;
    }
    if ( not $data->{$orgName} ) {
      print STDERR "$0: WARNING: Could not find \"$orgName\" in NCBI taxonomy;".
	" skipping.\n";
    } else {
      opendir DOT, "$genomeDataDir/$subdir";
      my @files = readdir DOT;
      closedir DOT;
      foreach my $file ( @files ) {
	if ( $file =~ /(\S+)\.gbk/ ) {
	  my $prefix = $1;
	  my $outFile = "$outdir/3_parsedTaxData/distributionOfTaxa.txt";
	  open OUT, ">>$outFile" 
	    or die("$0: FATAL: Can't open $outFile for appending.\n");
	  foreach my $taxon ( @taxa ) {
	    if ($taxon eq 'no rank' ) {
	      last;
	    }
	    my $resultString = '';
	    $resultString .= "$taxon\t";
	    my $taxVal = '';
	    foreach my $fieldID ( keys %{$data->{$orgName}} ) {
	      if ( $data->{$orgName}->{$fieldID}->{'label'} eq $taxon ) {
		$taxVal = $data->{$orgName}->{$fieldID}->{'value'};
		last;
	      }
	    }
	    if ( $taxVal ne '' ) {
	      $resultString .= "$taxVal\t";
	      $resultString .= "$prefix ";
	      my $speciesName;
	      foreach my $fieldID ( keys %{$data->{$orgName}} ) {
		if ( $data->{$orgName}->{$fieldID}->{'label'} eq 'species' ) {
		  $speciesName = $data->{$orgName}->{$fieldID}->{'value'};
		  last;
		}
	      }
	      $resultString .= "\($speciesName\)\t";
	      $resultString .= "$dirPrefix\n";
	      print OUT $resultString;
	    }
	  }
	  close OUT;
	} # if file =~ .icm
      } # foreach file in subdir
    } # do we have data for this species?
  } # have we got a valid organism subdirectory?
} # iterator on subs of genomeDataDir
exit 0;
