#!/usr/bin/perl

use strict;
$| = 1;
my $inFile = 'taxonomyData/1_fromNCBI/bacteria_taxonomy_stripped.html';
open IN, "<$inFile" or die("$0: FATAL: Can't open $inFile for reading.\n");
my $currentHierarchy = {};
my @taxStack = ();
my $lastEntityType = '';
my $lastEntityName = '';
my $outFile = 'taxonomyData/2_flatTaxFiles/flat_bacterial_taxonomy.txt';
open OUT, ">$outFile" or die("$0: FATAL: Can't open $outFile for writing.\n");
print OUT "---\n";
while ( my $line = <IN> ) {
  if ( $line =~ /<li.*?><a\s+title\=\"(.+?)\">(.+?)<\/a>/ ) {
    $lastEntityType = $1;
    $lastEntityName = $2;
    if ( $lastEntityType =~ /\"/ or $lastEntityName =~ /\"/ ) {
      die("$0: FATAL: Parse error: $lastEntityType $lastEntityName\n");
    }
    print OUT "NAME: \"$lastEntityName\"\n";
    print OUT "TYPE: \"$lastEntityType\"\n";
    foreach my $entityType ( @taxStack ) {
      print OUT "$entityType\: \"$currentHierarchy->{$entityType}\"\n";
    }
    print OUT "$lastEntityType: \"$lastEntityName\"\n";
    print OUT "---\n";
  } elsif ( $line =~ /<ul>/ and $lastEntityType ne '' ) {
    $currentHierarchy->{$lastEntityType} = $lastEntityName;
    push @taxStack, $lastEntityType;
  } elsif ( $line =~ /<\/ul>/ ) {
    my $removeEntityType = pop @taxStack;
    delete $currentHierarchy->{$removeEntityType};
  }
}
close OUT;
close IN;




