#!/usr/bin/perl
use strict;
$| = 1;
my ($outdir, $domain) = @ARGV;

die "$0: FATAL: outdir and/or domain not provided\n" 
  unless ($outdir && $domain);
die "$0: FATAL: domain can only be one of archaea, bacteria: $domain\n"
  unless ($domain eq "bacteria" || $domain eq "archaea");

my $inFile = "$outdir/1_fromNCBI/${domain}_taxonomy.html";
open IN, "<$inFile" or die("$0: FATAL: Can't open $inFile for reading.\n");
my $outFile = "$outdir/1_fromNCBI/${domain}_taxonomy_stripped.html";
open OUT, ">$outFile" or die("$0: FATAL: Can't open $outFile for writing.\n");

my $classLevels = {};
while ( my $line = <IN> ) {
  $line =~ s/<a(\s+)/<a$1/gi;
  $line =~ s/title\=/title\=/gi;
  $line =~ s/\/a>/\/a>/gi;
  if ( $line =~ /<li.*?><a\s+title\=\"(.+?)\"/i and $line !~ /superkingdom/ ) {
    my $classLevel = $1;
    $classLevels->{$classLevel} += 1;
    $line =~ s/\s+href\=\"(.*?)\"//gi;
    $line =~ s/<strong>//gi;
    $line =~ s/<\/strong>//gi;
    $line =~ s/<em>//gi;
    $line =~ s/<\/em>//gi;
    $line =~ s/<big>//gi;
    $line =~ s/<\/big>//gi;
    $line =~ s/(.)<li/$1\n<li/gi;
    $line =~ s/(.)<ul/$1\n<ul/gi;
    $line =~ s/(.)<\/li/$1\n<\/li/gi;
    $line =~ s/(.)<\/ul/$1\n<\/ul/gi;
    $line =~ s/<small>(.+?)<\/small>//gi;
    $line =~ s/\&nbsp\;/ /gi;
    $line =~ s/<li\s+type\=\"?.+?\"?>/<li>/gi;
    $line =~ s/<li\s+type\=.+?>/<li>/gi;
    $line =~ s/\s+compact\=\"?compact\"?//gi;
    $line =~ s/\s+compact>/>/gi;
    print OUT $line;
  } elsif ( $line =~ /\/ul>/i ) {
    $line =~ s/<\/li><\/ul>/<\/li>\n<\/ul>/gi;
    $line =~ s/\/ul>/\/ul>/gi;
    print OUT $line;
  } elsif ( $line =~ /<ul\s+compact/i ) {
    $line =~ s/ul\s+compact(\=\"compact\")?/ul/gi;
    print OUT $line;
  }
}
close OUT;
close IN;
exit 0;
