#!/usr/bin/perl
use strict;
$| = 1;

my $outdir = shift @ARGV;
die "$0: FATAL: outdir not provided\n" unless ($outdir);

system("mkdir -p $outdir/1_fromNCBI") == 0 
  or die "$0: FATAL: Cannot mkdir $outdir: $?\n" 
  unless (-d "$outdir/1_fromNCBI");

system("wget -O $outdir/1_fromNCBI/bacteria_taxonomy.html " . 
       '"http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Undef&id=2&lvl=50&srchmode=1&keep=1&unlock" >& /dev/null') == 0 
  or die "$0: FATAL: could not retrieve bacterial taxonomy from NCBI: $?\n";
system("wget -O $outdir/1_fromNCBI/archaea_taxonomy.html " . 
       '"http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Undef&id=2157&lvl=50&srchmode=1&keep=1&unlock" >& /dev/null') == 0 
  or die "$0: FATAL: could not retrieve archaeal taxonomy from NCBI: $?\n";
exit 0;
