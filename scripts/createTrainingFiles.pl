#!/usr/bin/perl
use strict;
$| = 1;

my ($rootDir, $windowLength) = @ARGV;
die("$0: FATAL: incorrect invocation. " . 
    "Usage: $0 <data directory> <windowLength>\n") 
  if ( not $windowLength or $windowLength < 0 );

opendir ROOT, $rootDir or die("$0: FATAL: Can't open $rootDir for scanning.\n");
my @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir ROOT;
closedir ROOT;
foreach my $sub ( @subs ) {
  if ( -d "$rootDir/$sub" ) {
    &recurseDirs("$rootDir/$sub");
  }
}

sub recurseDirs {
  my $root = shift;
  opendir DOT, $root or die("$0: FATAL: Can't open $root for scanning.\n");
  my @subs = sort { $a cmp $b } readdir DOT;
  closedir DOT;
  my $foundGBK = 0;
  my $foundFNA = 0;
  foreach my $sub ( @subs ) {
    if ( $sub =~ /\.fna$/ ) {
      $foundFNA = 1;
    } elsif ( $sub =~ /\.gbk$/ ) {
      $foundGBK = 1;
    }
  }
  if ( $foundFNA and $foundGBK ) {
    # We're in a bottom-level directory of the type that we want to process.  
    # Extract training sequences and save to a new file.
    foreach my $file ( @subs ) {
      if ( $file =~ /\.fna$/ ) {
	$file =~ /(\S+)\.fna/;
	my $prefix = $1;
	if ( -e "$root/$prefix\.gbk" and 
	     not -e "$root/$prefix\.${windowLength}bpGenomicTrainingSeqs.txt" ){
	  # We haven't built a training file for this FASTA file yet.  Do so.
	  my $trainHeaders = {};
	  my $trainSeqs = {};
	  my $seqIndex = 0;
	  my $mainSeq = &scanSequence("$root/$file");
	  if ( length($mainSeq) < $windowLength ) {
	    die("$0: FATAL: Length of $prefix is only " . length($mainSeq) . 
		" bp, which is smaller than the window you specified.\n");
	  }
	  # Grab all <windowLength>-bp subsequences from the genome, 
	  # overlapping by $windowLength/2.
	  my $index = 0;
	  while ( $index < length($mainSeq) - $windowLength ) {
	    my $subseq = substr($mainSeq, $index, $windowLength);
	    $trainHeaders->{$seqIndex} = ">$prefix\.$seqIndex " . ($index + 1) .
	      " " . ($index + $windowLength) . "\n";
	    $trainSeqs->{$seqIndex} = &fastaFy($subseq);
	    $seqIndex++;
	    $index += int($windowLength / 2);
	  }
	  my $subseq = substr($mainSeq, $index);
	  $trainHeaders->{$seqIndex} = ">$prefix\.$seqIndex " . ($index + 1) . 
	    " " . ($index + length($subseq)) . "\n";
	  $trainSeqs->{$seqIndex} = &fastaFy($subseq);
	  $seqIndex++;
	  my $targetLocation = "$root/$prefix\.$windowLength" . 
	    "bpGenomicTrainingSeqs.txt";
	  open OUT, ">$targetLocation" 
	    or die("$0: FATAL: Can't open $targetLocation for writing.\n");
	  foreach my $currentIndex ( sort { $a <=> $b } keys %{$trainHeaders} ){
	    print OUT $trainHeaders->{$currentIndex};
	    print OUT $trainSeqs->{$currentIndex};
	  }
	  close OUT;
	  print "   ...created \"$targetLocation\"...\n";
	} # end if ( there's no training file to accompany current .fna file )
      } # end if ( current file is a .fna file )
    } # end ( file-iterator on current subdirectory )
  } # end if ( $foundFNA && $foundGBK: this is a bottom-level subdirectory )
}

sub scanSequence {
  my $inFile = shift;
  open IN, "<$inFile" or die("$0: FATAL: Can't open $inFile for reading.\n");
  my $seq = '';
  while ( my $line = <IN> ) {
    chomp $line;
    if ( $line !~ /^>/ and $line =~ /\S/ ) {
      $line =~ s/\s//g;
      $seq .= $line;
    }
  }
  close IN;
  return $seq;
}

sub fastaFy {
  my $seq = shift;
  my $index = 0;
  my $result = '';
  while ( length($seq) - $index > 60 ) {
    my $subseq = substr($seq, $index, 60);
    $result .= "$subseq\n";
    $index += 60;
  }
  my $finalseq = substr($seq, $index);
  $result .= "$finalseq\n";
  return $result;
}

