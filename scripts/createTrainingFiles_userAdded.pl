#!/usr/bin/perl
use strict;
$| = 1;

my ($seqDir, $windowLength) = @ARGV;
die("$0: FATAL: incorrect invocation. " . 
    "Usage: $0 <data directory> <windowLength>\n") 
  if ( not $windowLength or $windowLength < 0 );

if ( -d "$seqDir" and $seqDir ne '.' and $seqDir ne '..' ) {
  &recurseDirs("$seqDir");
}

sub recurseDirs {
  my $root = shift;
  opendir DOT, $root 
    or die("$0: recurseDirs: FATAL: Can't open $root for scanning.\n");
  my @subs = sort { $a cmp $b } readdir DOT;
  closedir DOT;
  my $foundFNA = 0;
  foreach my $sub ( @subs ) {
    if ( $sub =~ /\.fna$/ ) {
      $foundFNA = 1;
    }
  }
  if ( $foundFNA ) {
    # We're in a bottom-level directory of the type that we want to process.  
    # Extract training sequences and save to a new file.
    foreach my $file ( @subs ) {
      if ( $file =~ /\.fna$/ ) {
	$file =~ /(\S+)\.fna/;
	my $prefix = $1;
	if ( not -e "$root/$prefix\.${windowLength}bpGenomicTrainingSeqs.txt" ){
	  # We haven't built a training file for this FASTA file yet.  Do so.
	  my $trainHeaders = {};
	  my $trainSeqs = {};
	  my $seqIndex = 0;
	  # We're allowing multifasta files.  Don't create chimeric segments 
	  # at the end.  Save multiple sequences as multiple sequences.
	  my $mainSeqArrayRef = &scanSequences("$root/$file");
	  my @mainSeqArray = @$mainSeqArrayRef;
	  foreach my $mainSeq ( @mainSeqArray ) {
	    if ( length($mainSeq) < $windowLength ) {
	      # Just skip it.
	    } else {
	      # Grab all <windowLength>-bp subsequences from the sequence, 
	      # overlapping by $windowLength/2.
	      my $index = 0;
	      while ( $index < length($mainSeq) - $windowLength ) {
		my $subseq = substr($mainSeq, $index, $windowLength);
		$trainHeaders->{$seqIndex} = ">$prefix\.$seqIndex " . 
		  ($index + 1) . " " . ($index + $windowLength) . "\n";
		$trainSeqs->{$seqIndex} = &fastaFy($subseq);
		$seqIndex++;
		$index += int($windowLength / 2);
	      }
	      my $subseq = substr($mainSeq, $index);
	      $trainHeaders->{$seqIndex} = ">$prefix\.$seqIndex " . 
		($index + 1) . " " . ($index + length($subseq)) . "\n";
	      $trainSeqs->{$seqIndex} = &fastaFy($subseq);
	      $seqIndex++;
	    }
	  }
	  my $targetLocation = "$root/$prefix\.$windowLength" . 
	    "bpGenomicTrainingSeqs.txt";
	  open OUT, ">$targetLocation" or die("$0: recurseDirs: FATAL: Can't open $targetLocation for writing.\n");
	  foreach my $currentIndex ( sort { $a <=> $b } keys %{$trainHeaders} ){
	    print OUT $trainHeaders->{$currentIndex};
	    print OUT $trainSeqs->{$currentIndex};
	  }
	  close OUT;
	} # end if (there's no training file to accompany the current .fna file)
      } # end if ( current file is a .fna file )
    } # end ( file-iterator on bottom-level subdirectory )
  } # end if ( this is a bottom-level subdirectory )
}

sub scanSequences {
  my $inFile = shift;
  open IN, "<$inFile" or die("$0: scanSequences: FATAL: Can't open $inFile for reading.\n");
  my @seqs = ();
  my $seq = '';
  my $first = 1;
  while ( my $line = <IN> ) {
    chomp $line;
    if ( $line =~ /^>/ and $first ) {
      $first = 0;
    } elsif ( $line =~ /^>/ ) {
      push @seqs, $seq;
      $seq = '';
    } else {
      $seq .= $line;
    }
  }
  push @seqs, $seq;
  close IN;
  return \@seqs;
}

sub fastaFy {
  my $seq = shift;
  my $index = 0;
  my $result = '';
  while ( length($seq) - $index > 60 ) {
    my $subseq = substr($seq, $index, 60);
    $result .= "$subseq\n";
    $index += 60;
  }
  my $finalseq = substr($seq, $index);
  $result .= "$finalseq\n";
  return $result;
}


