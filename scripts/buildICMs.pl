#!/usr/bin/perl

$| = 1;
use strict;
use FindBin qw/$Bin/;
use lib "$Bin";
use POE qw(Wheel::Run Filter::Reference);

my ($rootDir, $outFile, $errFile, $n_threads, $debug) = @ARGV;
$n_threads = 2 unless $n_threads;

close STDERR;
open STDERR, '>>', $errFile 
  or die("$0: FATAL: Couldn't internally redirect STDERR to \"$errFile\".\n");
die("Usage: $0 <root directory for search> <output log> <error log>\n") 
  unless $errFile;
open OUT, ">>$outFile" 
  or die("$0: FATAL: Can't open $outFile for appending.\n");
opendir IN, $rootDir 
  or die("$0: FATAL: Couldn't open $rootDir for scanning.\n");
my @dirs = grep { !/^\./ } sort { $a cmp $b } readdir IN;
closedir IN;
our $doneCount = 0;

# First, count the number of ICMs to be built so we can give the user an idea about our progress as we build new ones.
my $toBuildCount = 0;
foreach my $dir ( @dirs ) {
  if ( -d "$rootDir/$dir" ) {
    &countUnbuiltICMs("$rootDir/$dir");
  }
}

if ( $toBuildCount > 0 ) {
  # Now build any unbuilt ICMs.
  my $plural = '';
  if ( $toBuildCount > 1 ) {
    $plural = 's';
  }
  my $date = `date`;
  chomp $date;
  print "[$date] Beginning construction of $toBuildCount IMM$plural (this " . 
    "will take a while if you haven't yet done it)...\n\n";
  print OUT "[$date] Beginning construction of $toBuildCount IMM$plural...\n\n";
  # use POE::Wheel to manage many threads
  POE::Session->create(inline_states => {_start    => \&start_tasks,
					 next_task => \&start_tasks,
					 task_result => \&handle_task_result,
					 task_done => \&handle_task_done,
					 sig_child => \&sig_child,
					}
		      );
  $poe_kernel->run();
  $date = `date`;
  chomp $date;
  print "\n[$date] ...done.  Built $doneCount IMM${plural}.\n";
  print OUT "\n[$date] ...done.  Built $doneCount IMM${plural}.\n";
}

sub start_tasks {
  my ($kernel, $heap) = @_[KERNEL, HEAP];
  while (keys( %{$heap->{task}} ) < $n_threads){
    print scalar(keys(%{$heap->{task}})) . " tasks running\n" if $debug;
    my $next_dir = shift @dirs;
    # if no more tasks be sure to wait unil all tasks are finished
    last unless $next_dir;
    print "Starting task for " . $next_dir . "...\n" if $debug;
    my $task = POE::Wheel::Run->new(
      Program      => sub { makeICMsForSubdir("$rootDir/$next_dir") },
      StdoutFilter => POE::Filter::Reference->new(),
      StdoutEvent  => "task_result",
      StderrEvent  => "task_debug",
      CloseEvent   => "task_done",
    );
    $heap->{task}->{$task->ID} = $task;
    $kernel->sig_child($task->PID, "sig_child");
  }
}
sub handle_task_result {
  my $result = $_[ARG0];
  $doneCount += $result->{generated};
  my $date = `date`;
  chomp $date;
  if ( $doneCount % 10 == 0 && $doneCount > 0 ) {
    print OUT "[$date]    ...finished $doneCount/$toBuildCount...\n";
  }
  if ( $doneCount % 100 == 0 && $doneCount > 0 ) {
    print "[$date]    ...finished $doneCount/$toBuildCount...\n";
  }
}
sub handle_task_done {
  my ($kernel, $heap, $task_id) = @_[KERNEL, HEAP, ARG0];
  delete $heap->{task}->{$task_id};
  $kernel->yield("next_task");
}
sub handle_task_debug {
  my $result = $_[ARG0];
  print "Debug: $result\n";
}
sub sig_child {
  my ($heap, $sig, $pid, $exit_val) = @_[HEAP, ARG0, ARG1, ARG2];
  my $details = delete $heap->{$pid};
  #warn "$$: Child $pid exited with exit val $exit_val";
}

sub countUnbuiltICMs {
  my $dir = shift;
  opendir DOT, "$dir" 
    or die("$0: FATAL: countUnbuiltICMs(): couldn't open $dir for scanning.\n");
  my @files = readdir DOT;
  closedir DOT;
  foreach my $file ( @files ) {
    if ( $file =~ /(.+)\.\d+bpGenomicTrainingSeqs.txt$/ ) {
      my $prefix = $1;
      if ( not -e "$dir/$prefix\.icm" ) {
	$toBuildCount++;
      }
    }
  }
}

sub makeICMsForSubdir {
  my $root = shift;
  my $filter = POE::Filter::Reference->new();
  opendir DOT, "$root" 
    or die("$0: FATAL: makeICMsForSubdir(): Can't open $root for scanning.\n");
  my @subs = sort { $a cmp $b } readdir DOT;
  closedir DOT;
  my $foundTrain = 0;
  foreach my $sub ( @subs ) {
    if ( $sub =~ /bpGenomicTrainingSeqs\.txt/ ) {
      $foundTrain = 1;
    }
  }
  my $generated = 0;
  if ( $foundTrain ) {
    # Found a training file.  Generate genomic ICMs from all training 
    # sequence files in this directory.
    foreach my $file ( @subs ) {
      if ( $file =~ /(.+)\.\d+bpGenomicTrainingSeqs\.txt/ ) {
	my $prefix = $1;
	if ( not -e "$root/$prefix\.icm" ) {
	  &generateICM($root, $prefix, $file);
	  $generated++;
	}
      }
    }
  }
  my %result = ( generated => $generated );
  my $output = $filter->put( [\%result] );
  print @$output;
}

sub generateICM {
  my $dir = shift;
  my $prefix = shift;
  my $trainFile = shift;
  my $command = "$Bin/icmCode/bin/build-icm -d 10 -w 12 -p 1 " . 
    "$dir/$prefix.icm < $dir/$trainFile";
  #sleep rand(3)+1; ## debug
  system("$command\n");
  my $icmSizeCheck = -s "$dir/$prefix.icm";
  if ( $icmSizeCheck == 0 ) {
    # Try once more.  Every once in a long while the ICM construction
    # call fails mysteriously, and will succeed if re-run.
    system($command);
    $icmSizeCheck = -s "$dir/$prefix.icm";
    if ( $icmSizeCheck == 0 ) {
      die("$0: generateICM(): FATAL: Could not construct nonzero-length ICM " .
  	  "\"$dir/$prefix.icm\".\n");
    }
  }
}

