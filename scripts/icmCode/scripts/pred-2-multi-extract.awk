#!/bin/awk -f
# Usage:  pred-2-multi-extract.awk
#   Read Glimmer3 prediction file and output it
#   in a format suitable for input to  multi-extract  program.


/^>/  {
  tag = substr ($1, 2);
  next;
}

{
  printf "%s\t%s\t%7d %7d %4d\n", $1, tag, $2, $3, $4;
}


