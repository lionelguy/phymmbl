#!/usr/bin/perl

my $seq_id = "";

while (<>) {
	if (m/^>(\S+)/) { 
		$seq_id = $1;
	} else {
		print $seq_id . " " . $_;
	}
}
