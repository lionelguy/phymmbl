#!/usr/bin/perl
use strict;
$| = 1;

my $rootDir = shift @ARGV;

die("$0: FATAL: incorrect invocation.  Usage: $0 <root directory for search>\n")
  unless $rootDir;
my @files;
# Scan the root data directory for .gbk files we'll want to parse, and save 
# them in @files.
opendir ROOT, $rootDir or die("$0: FATAL: Can't open $rootDir for scanning.\n");
my @dirs = sort { $a cmp $b } grep { !/^\./ } readdir ROOT;
closedir ROOT;

foreach my $dir ( @dirs ) {
  if ( -d "$rootDir/$dir" ) {
    opendir SUB, "$rootDir/$dir" 
      or die("$0: FATAL: Can't open $rootDir/$dir for scanning.\n");
    my @subFiles = readdir SUB;
    closedir SUB;
    foreach my $subFile ( @subFiles ) {
      if ( $subFile =~ /\.gbk$/ ) {
	my $fastaCorrelate = $subFile;
	$fastaCorrelate =~ s/\.gbk$/\.fna/;
	$fastaCorrelate = "$rootDir/$dir/$fastaCorrelate";
	# Save each GenBank file that doesn't have a corresponding .fna file.
	if ( not -e $fastaCorrelate ) {
	  $subFile = "$rootDir/$dir/$subFile";
	  push @files, $subFile;
	}
      }
    }
  }
}

# Save the nucleotide sequences into .fna files.
foreach my $file ( @files ) {
  if ( $file =~ /(\S+)\.gbk/ ) {
    my $prefix = $1;
    open IN, "<$file" or die("$0: FATAL: Can't open $file for reading.\n");
    # First, grab the defline.
    my $done = 0;
    my $recording = 0;
    my $defLine = '';
    while ( not $done and my $line = <IN> ) {
      chomp $line;
      $line =~ s/\s*$//;
      if ( $line =~ /^DEFINITION\s*(.*)/ ) {
	$defLine .= $1;
	$recording = 1;
      } elsif ( $line =~ /^ACCESSION/ ) {
	$done = 1;
	$recording = 0;
      } elsif ( $recording ) {
	$line =~ s/^\s*//;
	$defLine .= ' ' . $line;
      }
    }
    # Next, skip to the nucleotide sequence.
    $done = 0;
    while ( not $done and my $line = <IN> ) {
      if ( $line =~ /^ORIGIN/ ) {
	$done = 1;
      }
    }
    # Establish the organism's name based on the containing directory.
    my $orgName = $prefix;
    $orgName =~ s/\/[^\/]+$//;
    $orgName =~ s/^.+\///;
    # Walk through the rest of the .gbk file, writing the nucleotide sequence 
    # to a corresponding .fna file as you go.
    my $outFile = "$prefix\.fna";
    open OUT, ">$outFile" or 
      die("$0: FATAL: Can't open $outFile for writing.\n");      
    print OUT ">$orgName \| $defLine\n";
    $done = 0;
    while ( not $done and my $line = <IN> ) {
      chomp $line;
      if ( $line =~ /^\/\// ) {
	$done = 1;
      } elsif ( $line =~ /^\s*\d+\s*(.+)$/ ) {
	my $tempLine = $1;
	$tempLine =~ s/\s+//g;
	$tempLine = uc($tempLine);
	print OUT "$tempLine\n";
      }
    }
    close OUT;
    close IN;
    print "   ...processed \"$file\"...\n";
  } # end if ( $file =~ /(\S+)\.gbk/ )
}
exit 0;

