#!/usr/bin/perl

$| = 1;
use strict;
use FindBin qw/$Bin/;
use lib "$Bin";
my $rootDir = shift;
die("$0: FATAL: incorrect invocation.  Usage: $0 <genome directory>\n") if not $rootDir;

my $silent = shift;
my $doneCount = 0;
# First, count the number of ICMs to be built so we can give the user an idea about our progress as we build new ones.

my $toBuildCount = 0;
if ( -d "$rootDir" and $rootDir ne '.' and $rootDir ne '..' ) {
  &countUnbuiltICMs("$rootDir");
}else{
  print STDERR "Invalid root folder *$rootDir*";
  exit;
}

if ( $toBuildCount > 0 ) {
  # Now build any unbuilt ICMs.
  my $plural = '';
  if ( $toBuildCount > 1 ) {
    $plural = 's';
  }
  my $date = `date`;
  chomp $date;
  print "\n\n[$date]   Beginning construction of $toBuildCount IMM$plural (this may take a while)...\n\n" unless $silent;
  # if ( -d "$rootDir" and $rootDir ne '.' and $rootDir ne '..' ) {
  &makeICMsForSubdir("$rootDir");
  # }
  my $date = `date`;
  chomp $date;
  print "\n[$date] ...done.  Built $doneCount IMM${plural}.\n\n" unless $silent;
}


sub countUnbuiltICMs {
  my $dir = shift;
  opendir DOT, "$dir" 
    or die("$0: FATAL: countUnbuiltICMs: couldn't open $dir for scanning.\n");
  my @files = readdir DOT;
  closedir DOT;
  foreach my $file ( @files ) {
    if ( $file =~ /(.+)\.\d+bpGenomicTrainingSeqs.txt$/ ) {
      my $prefix = $1;
      if ( not -e "$dir/$prefix\.icm" ) {
        $toBuildCount++;
      }
    }
  }
}

sub makeICMsForSubdir {
  my $root = shift;
  opendir DOT, "$root" or die("$0: FATAL: makeICMsForSubdir: Can't open $root for scanning.\n");
  my @subs = readdir DOT;
  closedir DOT;
  my $foundTrain = 0;
  foreach my $sub ( @subs ) {
    if ( $sub =~ /bpGenomicTrainingSeqs\.txt/ ) {
      $foundTrain = 1;
    }
  }
  if ( $foundTrain ) {
    # Found a training file.  Generate genomic ICMs from all training 
    # sequence files in this directory.
    foreach my $file ( @subs ) {
      if ( $file =~ /(.+)\.\d+bpGenomicTrainingSeqs\.txt/ ) {
	my $prefix = $1;
	if ( not -e "$root/$prefix\.icm" ) {
	  &generateICM($root, $prefix, $file);
	}
      }
    }
  }
}


sub generateICM {
  my $dir = shift;
  my $prefix = shift;
  my $trainFile = shift;
  my $command = "$Bin/icmCode/bin/build-icm -d 10 -w 12 -p 1 $dir/$prefix.icm < $dir/$trainFile";
  system("$command\n");
  my $icmSizeCheck = -s "$dir/$prefix.icm";
  if ( $icmSizeCheck == 0 ) {
    # Try once more.  Every once in a long while the ICM construction
    # call fails mysteriously, and will succeed if re-run.
    system($command);
    $icmSizeCheck = -s "$dir/$prefix.icm";
    if ( $icmSizeCheck == 0 ) {
      die("$0: generateICM: FATAL: Could not construct nonzero-length ICM \"$dir/$prefix.icm\".\n");
    }
  }
  $doneCount++;
  if ( not $silent and $doneCount % 10 == 0 ) {
    my $date = `date`;
    chomp $date;
    print "[$date]    ...finished $doneCount/$toBuildCount...\n";
  }
}
