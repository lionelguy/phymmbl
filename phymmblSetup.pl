#!/usr/bin/perl

=head1 SYNOPSIS

phymmblSetup.pl - Perform setup and update of databases necessary to run phymmBL

=head1 USAGE

phymmblSetup.pl -g|--genbank-mirror <path> [-d|--database <path>] [-t|--threads <int>]

=head1 INPUT

=head2 -g|--genbank-mirror <path>

The path to a genbank mirror, i.e. a folder with subfolders named after the DEFINITION field of the genbank file, and which contains one genbank file per replicon. The script genbankMirror.pl can generate such a folder.

=head2 [-d|--database <path>]

The path where the phymmbl database is/will be stored. This includes logs, genomeData, taxonomyData and blastData folders.

=head2 [-t|--threads <int>]

Number of threads to use. Minimum is 2 (default).

=head1 AUTHOR

Arthur Brady
Lionel Guy (lionel.guy@imbim.uu.se)

=head1 DATE

2015-06-04 for the revised version by Lionel Guy

=head1 COPYRIGHT

Copyright (c) 2009-2012 Arthur Brady; 
          (c) 2015 Arthur Brady, Lionel Guy

=head1 LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=cut

$| = 1;
use strict;
use warnings;
use Pod::Usage;
use Cwd;
use Net::FTP;
use Getopt::Long;
use File::stat; 
use FindBin qw/$Bin/;
use lib "$Bin/scripts";
#use File::Spec;

my $gbmirror;
my $datapath = getcwd;
my $help;
my $nthreads = 2;
GetOptions('g|genbank-mirror=s' => \$gbmirror,
	   'd|database=s'       => \$datapath,
	   't|threads=i'        => \$nthreads,
	   'h|help'             => \$help,
	  );

pod2usage(-exitval => 1, -verbose => 2) if $help;
# Checks that the genbank mirror exists, is readable and contains files
pod2usage(-exitval => 1, -message => "No genbank mirror at $gbmirror")
  unless (-d $gbmirror && -r $gbmirror && &checkRefSeqData($gbmirror));
# Nthreads can be only 2 or more
$nthreads = 2 if ($nthreads < 2);
# Verifies that the database folder is writable if exists and creates it other-
# wise
if (-e $datapath){
  pod2usage(-exitval => 2, -message => 
	    "Path $datapath is not a directory or not writable") 
    unless (-d $datapath && -w $datapath);
} else {
  system("mkdir -p $datapath") == 0 
    or die "FATAL: Cannot mkdir $datapath: $?\n";
}

my $logDir = "$datapath/logs/setup";
system("mkdir -p $logDir") == 0
  or die "FATAL: Cannot mkdir $logDir: $?\n";

# 0. Figure out whether to obtain a new copy of the RefSeq genomic database,
# use an existing one, or refresh existing data with new data from RefSeq.
# Store in $newCopy 
my $newCopy = &checkDataSource($datapath);

# OVERWRITE genome database with new data ($newCopy == 1): We've been
# told to completely overwrite existing genome data with current RefSeq
# data, treating this as a first-time installation.
&removeOldDatabase("$datapath/genomeData") if ($newCopy == 1);
# --> OR <--
# REFRESH existing genome database with changes from RefSeq since last 
# update ($newCopy == 2).
if ($newCopy > 0){
  &updateDatabaseFromMirror($gbmirror, $datapath);
}

# --> OR <--
# USE EXISTING genome database ($newCopy == 0).  This is essentially
# a cleanup operation.

# 1. Rename genome directories - in the existing genome database - as 
#    necessary.
&renameNewRefSeqDirs("$datapath/genomeData");

# 2. Generate metafiles describing the RefSeq data, using NCBI's taxonomy 
#    data to help.
&generateRefSeqMetafiles("$datapath/genomeData", "$datapath/taxonomyData");
&parseNCBITaxonomy("$datapath/genomeData", "$datapath/taxonomyData");

# 3. Extract FASTA sequences from the GenBank files as necessary.
&genbankToFasta("$datapath/genomeData");

# 4. Create training files for the ICMs as necessary.
&createTrainingFiles("$datapath/genomeData");

# 5. Build the local BLAST database from the RefSeq files.
&generateBlastDB($datapath);

# 6. Build/update the ICM suite.
&buildICMs("$datapath/genomeData");

###########################################################
# SUBROUTINES
###########################################################
sub checkDataSource {
  my ($path) = @_;
  my $newCopy;
  if ( &checkRefSeqData("$path/genomeData") == 1 ) {
    print "\nLocal genome database detected. ";
    print "Please choose one of the following:\n\n";
    print "Use (E)xisting genome data without changing it\n\n";
    print "(R)efresh database with newest RefSeq data (will not alter ";
    print "genomes which haven't been changed in the RefSeq database since ";
    print "Phymm was installed)\n\n";
    print "completely (O)verwrite existing genome data with fresh/current "; 
    print "copy from RefSeq\n\n";
    print "[Default is (E)]: ";
    my $response = <STDIN>;
    chomp $response;
    while ( length($response) > 1
            or ( length($response) == 1 and $response !~ /[eErRoO]/ ) ){
      print "\nPlease enter E (default: use existing genome data), "; 
      print "R (refresh with newest data) or O (overwrite with new copy): ";
      $response = <STDIN>;
      chomp $response;
    }
    print "\n";
    if ( $response =~ /[oO]/ ) {
      # Wipe genomeData.  If the _userAdded subdirectory's in there, this 
      # sub should leave it alone.
      $newCopy = 1;
    }
    elsif ( $response =~ /[rR]/ ) {
      # Refresh data with genomes added since install, and update genomes 
      # which've been altered since install.
      $newCopy = 2;
    }
    else {
      # We're going to use existing data.
      $newCopy = 0;    # (reaffirming default value)
    }
  }
  else {
    # In this case, $checkRefSeqData() has returned 0 and there's no data
    # in $path/genomeData. Do a fresh db from mirror, create folder if needed
    # No need to remove db, thus setting newCopy to 2.
    system("mkdir -p $path/genomeData") == 0
      or die "FATAL: Cannot create folder $path/genomeData: $?";
    $newCopy = 2
  }
  return $newCopy;
}
# Remove old database, preserving the _userAdded folder
sub removeOldDatabase {
  my ($path) = @_;
  my $removeLog = "$logDir/00_remove_old_database__out.txt";
  open OUT, ">$removeLog"
    or die("FATAL: Can't open $removeLog for writing.\n");
  my $removeErr = "$logDir/01_remove_old_database__err.txt";
  open ERR, ">$removeErr"
    or die("FATAL: Can't open $removeErr for writing.\n");
  print "Removing old database $path...";
  print OUT "Removing old database $path...";
  opendir DATA, "$path";
  my @subs = readdir DATA;
  closedir DATA;
  foreach my $sub (@subs) {
    if ( -d "$path/$sub" && $sub !~ /^\./ 
	 && $sub !~ /^_userAdded/ ) {
      system("rm -rf $path/$sub") == 0
	or die "FATAL: Could not remove $path/$sub: $!\n";
    }
  }
  print "done\n";
  print OUT "done\n";
}
# Check for the existence of a gzipped tarball from the RefSeq FTP server,
# as well as any content in the passed directory.
sub checkRefSeqData {
  my ($path) = @_;
  # If there's *anything at all* in the directory, assume 
  # it's a genomic library and return 1.
  return 0 unless (-d $path);
  opendir DATA, "$path" or die "Could not open dir $path\n";
  my @subs = readdir DATA;
  closedir DATA;
  foreach my $sub (@subs) {
    if ( -d "$path/$sub" && $sub !~ /^\./ 
	 && $sub !~ /^_userAdded/ ) {
      return 1;
    }
  }
  # The genomeData directory is empty.  Return 0.
  return 0;
}
# Download a current tarball from RefSeq.
sub downloadNewRefSeqData {
  my $downloadLog = "$logDir/00_download_refseq_tarball__out.txt";
  open OUT, ">$downloadLog"
    or die("FATAL: Can't open $downloadLog for writing.\n");
  my $downloadErr = "$logDir/01_download_refseq_tarball__err.txt";
  open ERR, ">$downloadErr"
    or die("FATAL: Can't open $downloadErr for writing.\n");
  my $ftpSite = 'ftp.ncbi.nih.gov';
  print "Connecting to $ftpSite...";
  print OUT "Connecting to $ftpSite...";
  my $ftp =
    Net::FTP->new( $ftpSite, Passive => 1, Debug => 0, Timeout => 2400 )
	or die "FATAL: Can't connect to $ftpSite: $@";
  $ftp->login()
    or (  print ERR "Can't log in: " . $ftp->message . "\n"
	  and close ERR
	  and close OUT
	  and die "FATAL: Can't log into $ftpSite: " . $ftp->message . "\n" );
  print "done.\n\n";
  print OUT "done.\n\n";
  my $targetDir = '/genomes/Bacteria';
  $ftp->cwd($targetDir)
    or ( print ERR "Can't change remote directory: "
	 . $ftp->message . "\n"
	 and close ERR
	 and close OUT
	 and die "FATAL: Can't change remote directory: "
	 . $ftp->message
	 . "\n" );
  my $targetFile = 'all.gbk.tar.gz';
  print "Downloading $targetFile (this will take a while; " . 
    "the file is over 2GB)...";
  print OUT "Downloading $targetFile (this will take a while; " . 
    "the file is over 2GB)...";
  $ftp->binary();
  $ftp->get( $targetFile, "./$targetFile" )
    or (  print ERR "Failed to download file: " . $ftp->message . "\n"
	  and close ERR
	  and close OUT
	  and die "FATAL: Failed to download file: " . $ftp->message . "\n" );
  print "done.\n\n";
  print OUT "done.\n\n";
  $ftp->quit;
  close OUT;
  close ERR;
}
# Unzip/untar the downloaded genome-data file into the genomeData directory.
sub expandNewRefSeqDatagenomeData {
  my $expansionLog = "$logDir/02_NEW_refseq_tarball_expansion__out.txt";
  open OUT, ">$expansionLog"
    or die("FATAL: Can't open $expansionLog for writing.\n");
  my $expansionErr = "$logDir/03_NEW_refseq_tarball_expansion__err.txt";
  print "Expanding downloaded genome data into genomeData/ for " . 
    "database overwrite (this'll take a while)...";
  print OUT "Expanding downloaded genome data into genomeData/ for " . 
    "database overwrite:\n\n";
  print OUT "Running command: \"tar -zxvf all.gbk.tar.gz -C genomeData " . 
      "> $expansionLog 2> $expansionErr\"\n\n";
  close OUT;
  system("tar -zxvf all.gbk.tar.gz -C genomeData > $expansionLog " . 
	 "2> $expansionErr");
  open OUT, ">>$expansionLog"
    or die("FATAL: Can't open $expansionLog for appending.\n");
  print "done.\n\nRemoving compressed genome data...";
  print OUT "done.\n\nRemoving compressed genome data...";
  system("rm -rf all.gbk.tar.gz");
  print "done.\n\n";
  print OUT "done.\n\n";
  close OUT;
}
# Unzip/untar the downloaded genome-data file into a temp directory for comparison/update.
sub expandNewRefSeqData_tempDir {
  my $expansionLog = "$logDir/02_UPDATE_refseq_tarball_expansion__out.txt";
  open OUT, ">$expansionLog"
    or die("FATAL: Can't open $expansionLog for writing.\n");
  my $expansionErr = "$logDir/03_UPDATE_refseq_tarball_expansion__err.txt";
  print  "Expanding downloaded genome data for database update " . 
    "(this'll take a while)...";
  print OUT "Expanding downloaded genome data for database update:\n\n";
  system("mkdir _tempGenomeData");
  print OUT "Running command: \"tar -zxvf all.gbk.tar.gz -C _tempGenomeData " .
    "> $expansionLog 2> $expansionErr\"\n\n";
  close OUT;
  system("tar -zxvf all.gbk.tar.gz -C _tempGenomeData > $expansionLog " . 
	 "2> $expansionErr");
  open OUT, ">>$expansionLog"
    or die("FATAL: Can't open $expansionLog for appending.\n");
  print "done.\n\nRemoving tarball...";
  print OUT "\ndone.\n\nRemoving tarball...";
  system("rm -rf all.gbk.tar.gz");
  print "done.\n\n";
  print OUT "done.\n\n";
  close OUT;
}
# Update database from a genbank mirror: 
# 1. list what's in the db, collect links & icm files
# 2. list what's in the gbmirror
# 3. for each folder in the gbmirror:
#    - if there already is a link, scan icm files: 
#       - if they are there and more recent, do nothing
#       - if they are there but older, delete all but gbk links
#    - if there is no link, create folder, or just put the links there 
#      if they don't exist
# 4. files in the db but not (anymore) in the mirror: do nothing (so far)
sub updateDatabaseFromMirror {
  my ($gbmirror, $datapath) = @_;
  my $updateLog = "$logDir/02_update_database_from_mirror__out.txt";
  open OUT, ">$updateLog"
    or die("FATAL: Can't open $updateLog for writing.\n");
  my $updateErr = "$logDir/03_update_database_from_mirror__err.txt";
  open ERR, ">$updateErr"
    or die("FATAL: Can't open $updateErr for writing.\n");
  print "Updating database $datapath from genbank mirror $gbmirror...";
  print OUT "Updating database $datapath from genbank mirror $gbmirror...";
  my $absgbmirror = File::Spec->rel2abs($gbmirror)
    or die "FATAL: Cannot get absolute path of $gbmirror: $!\n";
  my %ingbmirror;
  # First list folders and icms in db
  my $indb = &listFoldersInDb($datapath, $gbmirror);
  # Now list folders and gbk files in the mirror
  opendir DATA, $gbmirror 
    or die "\nFATAL: Could not open GB mirror: $gbmirror: $!\n";
  my @subs = readdir DATA;
  closedir DATA;
  foreach my $sub (@subs) {
    # For each subfolder, list files
    next unless (-d "$gbmirror/$sub" and $sub !~ /^\./);
    opendir DATA, "$gbmirror/$sub" 
      or die "\nFATAL: Could not open GB mirror sub: $gbmirror/$sub: $!\n";
    my @files = readdir DATA;
    closedir DATA;
    # Is there a link in db?
    if ($indb->{$sub}){
      # There is a link. Go through all gbk files
      my $dbsub = $indb->{$sub}{'dbsub'};
      foreach my $file (@files){
	next unless ($file =~ /\.gbk$/ && -s "$gbmirror/$sub/$file");
	(my $accession = $file) =~ s/\.gbk//;
	# This is a gbk file. Is the corresponding icm present?
	if ($indb->{$sub}{'icms'}{$accession}){
	  my @gbkstats = stat $file;
	  # Is the icm more recent than the gbk?
	  if ($indb->{$sub}{'icms'}{$accession} > $gbkstats[9]){
	    # do nothing
	  } else {
	    # remove icm file
	    unlink "$datapath/genomeData/$dbsub/$accession.icm" 
	      or die "FATAL: Could not rm $gbmirror/$sub/$accession.gbk: $!\n";
	  }
	} else {
	  # There is no icm present: make sure there is a link to gbk file
	  my $symlink = "$datapath/genomeData/$dbsub/$accession.gbk";
	  symlink "$gbmirror/$sub/$accession.gbk", $symlink 
	    or die "FATAL: Cannot symlink $symlink: $!\n" unless (-l $symlink);
	}
      } # end for each file in sub
    } else {
      # There is no link. Create folder in db, make links
      mkdir "$datapath/genomeData/$sub" 
	or die "FATAL: Cannot mkdir $datapath/genomeData/$sub: $!\n" 
	  unless -e "$datapath/genomeData/$sub";
      # Make link to folder: using gbsub
      symlink "$absgbmirror/$sub", "$datapath/genomeData/$sub/$sub"
	  or die "FATAL: Cannot symlink $absgbmirror/$sub: $!\n" 
	    unless -e "$absgbmirror/$sub";
      foreach my $file (@files){
	next unless ($file =~ /\.gbk$/ && -s "$gbmirror/$sub/$file");
	(my $accession = $file) =~ s/\.gbk//;
	my $symlink = "$datapath/genomeData/$sub/$accession.gbk";
	my $target = "$absgbmirror/$sub/$file";
	symlink $target, $symlink 
	  or die "FATAL: Cannot symlink $symlink: $!\n" unless (-l $symlink);
      }
    } # end if there is a link
  } # end foreach sub folders
  print "done\n";
  print OUT "done\n";
}
sub listFoldersInDb {
  my ($datapath, $gbmirror) = @_;
  my %indb;
  my $absgbmirror = File::Spec->rel2abs($gbmirror) 
    or die "FATAL: Cannot get absolute path of $gbmirror: $!\n";
  opendir DATA, "$datapath/genomeData" 
    or die "\nFATAL: Could not open folder $datapath/genomeData: $!\n";
  my @subs = readdir DATA;
  closedir DATA;
  foreach my $sub (@subs) {
    # For each subfolder, list files
    my @icms;
    if ( -d "$datapath/genomeData/$sub" && $sub !~ /^\./ 
	 && $sub !~ /^_userAdded/ ) {
      opendir DOT, "$datapath/genomeData/$sub" 
	or die "\nFATAL: Could not open folder $datapath/genomeData/$sub: $!\n";
      my @files = readdir DOT;
      close DOT;
      my $gbsub;
      # Record .icm files, and use the link as hash key
      foreach (@files){
	if (/\.icm$/ && -s "$datapath/genomeData/$sub/$_"){
	  s/\.icm$//;
	  push @icms, $_;
	} elsif (-l $_){
	  my $link = readlink($_) or die "FATAL: Could not read link $_: $!\n";
	  # Make sure that this links to the gb mirror. Ignores otherwise.
	  my $abslink = File::Spec->rel2abs($link)
	    or die "FATAL: Cannot get absolute path of $link: $!\n";
	  if ($abslink =~ /^$absgbmirror/){
	    ($gbsub = $abslink) =~ s/^$absgbmirror//;
	  }
	}
      }
      # Push files to %indb if the link is valid and links to GB
      die "FATAL: Already a link to GB mirror present in $sub\n" 
	if $indb{$gbsub};
      if ($gbsub){
	$indb{$gbsub}{'dbsub'} = $sub;
	foreach my $icm (@icms){
	  my @stats = stat $icm;
	  $indb{$gbsub}{'icms'}{$icm} = $stats[9];
	}
      }
    }
  }
  return \%indb
}
# Standardize the names of the RefSeq organism directories to match the names given in the .gbk files.
sub renameNewRefSeqDirs {
  # Get the name of the genome data directory to scan.
  my $dataDir = shift;
  # Load known GenBank SOURCE-field inconsistencies for later processing.
  my $GBerrors = {};
  #&loadGBerrors($GBerrors);
  my $warningsIssued = 0;
  my $renameLog = "$logDir/04_rename_genome_directories__out.txt";
  if ( $dataDir eq '_tempGenomeData' ) {
    $renameLog =~ s/04/06/;
    $renameLog =~ s/_genome/_temp_genome/;
  }
  open OUT, ">$renameLog"
    or die("FATAL: Can't open $renameLog for writing.\n");
  my $renameErr = "$logDir/05_rename_genome_directories__err.txt";
  if ( $dataDir eq '_tempGenomeData' ) {
    $renameErr =~ s/05/07/;
    $renameErr =~ s/_genome/_temp_genome/;
  }
  system("rm -f $renameErr") == 0 
    or die "Cannot rm $renameErr: $?\n";
  print "Standardizing organism directory names and data in $dataDir...";
  print OUT
    "Standardizing organism directory names and data in $dataDir...\n\n";
  opendir DATA, "$dataDir"
    or die("FATAL: Can't open $dataDir for scanning.\n");
  my @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DATA;
  closedir DATA;
  # Iterate over subdirectories of genomeData.
  foreach my $sub (@subs) {
    # Organism-wide variable.  Used for sanity checking.
    my $orgName = '';
    if ( -d "$dataDir/$sub" ) {
      ## file removal at the mirror level. The whole thing should go
      # opendir SUB, "$dataDir/$sub"
      # 	or die("FATAL: Can't open $dataDir/$sub for scanning.\n");
      # my @files = readdir SUB;
      # closedir SUB;
      # Delete unwanted files.
      #      foreach my $file (@files) {
	## Not sure this is relevant... remove
	# if ( $file =~ /\%$/ ) {
	#   # This file's been obsolesced by NCBI, but for whatever reason 
	#   # remained in the distribution.  Kill it.
	#   system("rm $dataDir/$sub/$file");
	# }
	## Commenting out, will probably disappear. Control at the mirror level
	# # We only want accessions beginning with NC_, all others are in varying 
	# # stages of unreadiness.
	# # Also, {NC_008265, NC_017594, NC_018285, NC_002179, NC_002180 and 
	# # NC_017732} are phages and not bacteria; delete them.
	# if ($file =~ /\.gbk$/
	#     and (  $file !~ /NC_/
	# 	   or $file =~ /NC_008265/
	# 	   or $file =~ /NC_017594/
	# 	   or $file =~ /NC_018285/
	# 	   or $file =~ /NC_002179/
	# 	   or $file =~ /NC_002180/
	# 	   or $file =~ /NC_017732/ )
	#    ){
	#   system("rm $dataDir/$sub/$file");
	# }
      #}  # end ( loop deleting unwanted files )
      ## end remove
      # Refresh the file list.  We may have deleted some.
      opendir SUB, "$dataDir/$sub"
	or die("FATAL: Can't open $dataDir/$sub for scanning.\n");
      my @files = readdir SUB;
      closedir SUB;
      # Go through each GenBank file in the current subdirectory of genomeData.
      # 1. Grab the (possibly multi-line) organism name from the SOURCE field
      # 2. If $orgName hasn't been set yet, set it to the current (retrieved) 
      #    organism name.
      # 3. If $orgName has been set, then compare it to the current (retrieved)
      #    organism name.
      #    If they don't match, it means there are different listings in the 
      #    SOURCE field across different files in the same subdirectory.  
      #    Check against a list of known GenBank inconsistencies and fix if 
      #    possible.  Otherwise, complain with a warning.
      foreach my $file (@files) {
	if ( $file =~ /\.gbk$/ ) {
	  my $fullFile = "$dataDir/$sub/$file";
	  open IN, "<$fullFile"
	    or die("FATAL: Can't open $fullFile for reading.\n");
	  my $found = 0;
	  my $recording = 0;
	  my $currentOrgName = '';
	  while ( not $found and my $line = <IN> ) {
	    chomp $line;
                        $line =~ s/\s*$//;
	    if ( $recording and $line !~ /^\s*ORGANISM/ ) {
	      $line =~ /^\s*(.*)/;
	      $currentOrgName .= ' ' . $1;
	    }
	    elsif ( $line =~ /^SOURCE\s*(.*)/ ) {
	      $currentOrgName = $1;
	      $recording = 1;
	    }
	    elsif ( $line =~ /^\s*ORGANISM/ ) {
	      $found = 1;
	    }
	  }
	  close IN;
	  # Sanity check on organism name.
	  $currentOrgName = &fixOrgName( $currentOrgName, $GBerrors );
	  if ( $orgName eq '' ) {
	    $orgName = $currentOrgName;
	  }
	  elsif ( $orgName ne $currentOrgName ) {
	    if ( $currentOrgName =~ /^$orgName/ ) {
	      # Do nothing; we want to keep the version that's a prefix of 
	      # the other.
	    }
	    elsif ( $orgName =~ /^$currentOrgName/ ) {
	      # Save the shorter name.
	      $orgName = $currentOrgName;
	    }
	    else {
	      # We really don't know how to resolve this.
	      open ERR, ">>$renameErr"
		or die(
		       "FATAL: Can't open $renameErr for appending.\n"
		      );
	      print ERR	"WARNING: Inconsistent source-organism information " . 
		"listed across multiple .gbk files in genome directory" . 
		  " \"$dataDir/$sub\": \"$orgName\" and " .
		    "\"$currentOrgName\".\n\n";
	      $warningsIssued = 1;
	      close ERR;
	    }
	  }
	}    # end ( filename check for /\.gbk$/ )
      }    # end ( file iterator on current subdirectory )
      # Check to ensure we haven't just emptied the current subdirectory.  
      # If we have, delete it.
      opendir SUB, "$dataDir/$sub"
	or die("FATAL: Can't open $dataDir/$sub for scanning.\n");
      @files = readdir SUB;
      closedir SUB;
      my $found = 0;
      foreach my $file (@files) {
	if ( $file ne '.' and $file ne '..' ) {
	  $found = 1;
	}
      }
      if ( not $found ) {
	# No files left in this subdirectory.  Delete it.
	system("rm -rf $dataDir/$sub\n") == 0 
	  or die "Cannot remove $dataDir/$sub: $?";
      }
      if ($found) {
	# The subdirectory isn't empty.  Continue.
	# If we're not dealing with a completely alien directory - i.e., if we
	# successfully found an $orgName - process the directory, otherwise 
	# ignore.
	if ( $orgName ne '' ) {
	  # Rename the current subdirectory to a standard name based on the 
	  # species name quoted in its .gbk files.
	  my $newSubName = &toDirName($orgName);
	  if ( $newSubName ne $sub ) {
	    # We've changed the directory name.
	    if ( not -e "$dataDir/$newSubName" ) {
	      # There's not yet a directory labeled with the new name.  Just 
	      # move the old one over.
	      my $tempSub = $sub;
	      $tempSub = &unixEscape($tempSub);
	      my $command =
		"mv $dataDir/$tempSub $dataDir/$newSubName\n";
	      system($command) == 0 
		or die "Cannot mv $dataDir/$tempSub: $?";
	      print OUT
		"   ...moved files from \"$sub\" to \"$newSubName\"...\n";
	    }
	    else {
	      # We've made a change, but the target directory (labeled with the
	      # new name) already exists; move the /files/ from the old dir to
	      # the new one.
	      my $tempSub = $sub;
	      $tempSub = &unixEscape($tempSub);
	      my $command =
		"mv $dataDir/$tempSub/* $dataDir/$newSubName\n";
	      system($command) == 0 
		or die "Cannot mv $dataDir/$tempSub/*: $?";
	      # Delete the now-empty directory labeled with the old name.
	      $command = "rm -rf $dataDir/$tempSub\n";
	      system($command) == 0 
		or die "Cannot rm $dataDir/$tempSub: $?";
	      print OUT
		"   ...moved files from \"$sub\" to \"$newSubName\"...\n";
	    }
	  } # end ( check to see if we changed the directory name from what 
	    # it was )
	} # end ( check to discover whether or not we've been able to establish 
	  #an $orgName )
      } # end ( check to ensure current subdirectory isn't empty )
    } # end ( filter on /^\./ )
  } # end foreach ( subdirectory in genomeData )
  print OUT "\ndone.\n\n";
  close OUT;
  print "done.";
  if ($warningsIssued) {
    print "\n\n   NOTE: non-fatal warnings were issued while standardizing " . 
      "names of\n   individual organisms' genome data directories; this is " . 
	"almost always\n   caused by a typo or syntax inconsistency in ". 
	  "GenBank-derived metadata.\n   Check the file \"$renameErr\" after\n".
	    "   setup completes to see details of these warnings.";
  }
  print "\n\n";
}
# Generate all needed metadata for later Phymm processing.
sub generateRefSeqMetafiles {
  my ($dataDir, $taxonomyDir) = @_;
  my $taxonomyLog = "$logDir/06_generate_taxonomic_metadata__out.txt";
  if ( $newCopy == 2 ) {
    $taxonomyLog =~ s/06/10/;
  }
  open OUT, ">$taxonomyLog"
    or die("FATAL: Can't open $taxonomyLog for writing.\n");
  my $taxonomyErr = "$logDir/07_generate_taxonomic_metadata__err.txt";
  if ( $newCopy == 2 ) {
    $taxonomyErr =~ s/07/11/;
  }
  system("rm -f $taxonomyErr") == 0 or die "Cannot rm $taxonomyErr: $?\n";
  print "Generating taxonomic metafiles...";
  print OUT "Generating taxonomic metafiles...\n\n";
  my $species = {};
  my $definition = {};
  #my $dataDir = 'genomeData';
  opendir DATA, "$dataDir"
    or die("FATAL: Can't open $dataDir for scanning.\n");
  my @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DATA;
  closedir DATA;
  # Go through each subdirectory of genomeData and grab the defline.  
  # Reconstruct the organism name from the directory name.
  print OUT "   Grabbing deflines from .gbk files...";
  foreach my $sub (@subs) {
    my $defLine = '';
    my $orgName = '';
    if ( -d "$dataDir/$sub" ) {
      opendir SUB, "$dataDir/$sub"
	or die("FATAL: Can't open $dataDir/$sub for scanning.\n");
      my @files = readdir SUB;
      closedir SUB;
      $orgName = &fromDirName($sub);
      foreach my $file (@files) {
	if ( $file =~ /(.+)\.gbk$/ ) {
	  my $prefix = $1;
	  my $fullFile = "$dataDir/$sub/$file";
	  open IN, "<$fullFile"
	    or die("FATAL: Can't open $fullFile for reading.\n");
	  my $found = 0;
	  $defLine = '';
	  my $recording = 0;
	  while ( not $found and my $line = <IN> ) {
	    chomp $line;
	    $line =~ s/\s*$//;
	    if ( $recording and $line !~ /^ACCESSION/ ) {
	      $line =~ /^\s*(.*)/;
	      $defLine .= ' ' . $1;
	    }
	    elsif ( $line =~ /^DEFINITION\s*(.*)/ ) {
	      $defLine = $1;
	      $recording = 1;
	    }
	    elsif ( $line =~ /^ACCESSION/ ) {
	      $found = 1;
	    }
	  }
	  $species->{$prefix} = $orgName;
	  $definition->{$prefix} = $defLine;
	}    # end ( filename check for /\.gbk$/ )
      }    # end ( file iterator on current subdirectory )
    }    # end ( filter on /^\./ )
  }    # end foreach ( subdirectory in genomeData )
  print OUT "done.\n";
  # Output all the saved data to an accession map file.
  system("mkdir -p $taxonomyDir/0_accessionMap") == 0 
    or die "Cannot mkdir $taxonomyDir/taxonomyData/0_accessionMap: $?\n"
      unless (-d "$taxonomyDir/0_accessionMap");
  my $idMapFile = "$taxonomyDir/0_accessionMap/accessionMap.txt";
  open IDMAP, ">$idMapFile"
    or die("FATAL: Can't open $idMapFile for writing.\n");
  print OUT "   Writing accession-to-species map to \"$idMapFile\"...";
  foreach my $prefix (sort { $species->{$a} cmp $species->{$b}
			       || $definition->{$a} cmp $definition->{$b}
			     } keys %$species ){
    my $orgName = $species->{$prefix};
    my $defLine = $definition->{$prefix};
    my $seqType = '';
    if ( $defLine =~ /chromosom/ or $defLine =~ /complete\s+genome/ ) {
      $seqType = 'chromosome';
    }
    elsif ( $defLine =~ /plasmid/ ) {
      $seqType = 'plasmid';
    }
    elsif ( $defLine =~ /complete\s+sequence/ ) {
      $seqType = 'chromosome';
    }
    else {
      $seqType = 'chromosome';
    }
    my $newSubName = &toDirName($orgName);
    print IDMAP "$newSubName\t$prefix\t$seqType\t$defLine\n";
  }
  close IDMAP;
  print OUT "done.\n";
  return 1;
}
sub parseNCBITaxonomy {
  my ($dataDir, $outdir) = @_;

  my $taxonomyLog = "$logDir/06_generate_taxonomic_metadata__out.txt";
  if ( $newCopy == 2 ) {
    $taxonomyLog =~ s/06/10/;
  }
  open OUT, ">>$taxonomyLog"
    or die("FATAL: Can't open $taxonomyLog for writing.\n");
  my $taxonomyErr = "$logDir/07_generate_taxonomic_metadata__err.txt";
  if ( $newCopy == 2 ) {
    $taxonomyErr =~ s/07/11/;
  }

  # Grab the NCBI taxonomy, parse, and save in a format we can cope with.
  print OUT "   Scraping NCBI taxonomy webpages for bacteria and archaea into" .
    " $outdir/1_fromNCBI/ ...";
  system("$Bin/scripts/taxonomyInfo/0_getNCBIpages.pl $outdir") == 0 
    or die "Something went wrong running 0_getNCBIpages.pl: $?";
  foreach my $domain ('archaea', 'bacteria'){
    print OUT "done.\n   Parsing HTML ($domain)...";
    system("$Bin/scripts/taxonomyInfo/1_stripHTML.pl $outdir $domain " . 
	   "2>> $taxonomyErr") == 0 
	     or die "Something went wrong running 1_stripHTML.pl: $?";
    print OUT "done.\n   Extracting taxonomic tree ($domain)...";
    system("$Bin/scripts/taxonomyInfo/2_getTree.pl $outdir $domain " . 
	   "2>> $taxonomyErr") == 0 
	     or die "Something went wrong running 2_getTree.pl: $?";
  }
  print OUT "done.\n   Creating flat taxonomy files...";
  system("$Bin/scripts/taxonomyInfo/3_configureTaxonomicData.pl $dataDir " . 
	 "$outdir 2>> $taxonomyErr") == 0 or 
	   die "Something went wrong running 3_configureTaxonomicData.pl: $?";
  print OUT "done.\n";
  print "done.\n\n";
  print OUT "\n...taxonomic metadata generation complete.\n";
  close OUT;
  return 1;
}
# Convert the .gbk files that came with the RefSeq distribution to corresponding .fna files which contain only the nucleotide sequence from the .gbk file.
sub genbankToFasta {
  my ($dataDir) = @_;

  my $fastaLog = "$logDir/08_genbank_to_fasta__out.txt";
  if ( $newCopy == 2 ) {
    $fastaLog =~ s/08/12/;
  }
  my $fastaErr = "$logDir/09_genbank_to_fasta__err.txt";
  if ( $newCopy == 2 ) {
    $fastaErr =~ s/09/13/;
  }
  open OUT, ">$fastaLog" or die("FATAL: Can't open $fastaLog for writing.\n");
  print "Extracting nucleotide sequences from .gbk files into .fna files " . 
    "as necessary...";
  print OUT "Extracting nucleotide sequences from .gbk files into .fna files " .
    "as necessary...\n\n";
  close OUT;
  system("$Bin/scripts/genbankToFasta.pl $dataDir >> $fastaLog 2> $fastaErr") 
    == 0 or die "Something went wrong running genbankToFasta.pl: $?\n";
  print "done.\n\n";
  open OUT, ">>$fastaLog"
    or die("FATAL: Can't open $fastaLog for appending.\n");
  print OUT "\ndone.\n";
  close OUT;
}
# Create training files for the ICMs if they don't already exist.
sub createTrainingFiles {
   my ($dataDir) = @_;

   my $trainFileLog = "$logDir/10_create_training_files__out.txt";
    if ( $newCopy == 2 ) {
        $trainFileLog =~ s/10/14/;
    }
    open OUT, ">$trainFileLog"
      or die("FATAL: Can't open $trainFileLog for writing.\n");
    my $trainFileErr = "$logDir/11_create_training_files__err.txt";
    if ( $newCopy == 2 ) {
        $trainFileErr =~ s/11/15/;
    }
    print "Creating training files for ICMs as necessary...";
    print OUT "Creating training files for ICMs as necessary...\n\n";
    close OUT;
    system("$Bin/scripts/createTrainingFiles.pl $dataDir 500 >> $trainFileLog ".
	   "2> $trainFileErr") == 0 or 
	     die "Something went wrong running createTrainingFiles.pl: $?\n" ;
    print "done.\n\n";
    open OUT, ">>$trainFileLog"
      or die("FATAL: Can't open $trainFileLog for appending.\n");
    print OUT "\ndone.\n";
    close OUT;
}
# Build the ICMs which form the core of Phymm's classification system
sub buildICMs {
  my ($genomedata) = @_;

  my $buildICMsLog = "$logDir/14_build_ICMs__out.txt";
  if ( $newCopy == 2 ) {
    $buildICMsLog =~ s/14/18/;
  }
  open OUT, ">$buildICMsLog"
    or die("FATAL: Can't open $buildICMsLog for writing.\n");
  my $buildICMsErr = "$logDir/15_build_ICMs__err.txt";
  if ( $newCopy == 2 ) {
    $buildICMsErr =~ s/15/19/;
  }
  system("rm -f $buildICMsErr");
  # First, check to see if the ICM code from the Glimmer package has been " . 
  # untarred and/or compiled yet.  Do whatever needs doing.
  if ( not -e "$Bin/scripts/icmCode/LICENSE" ) {
    # The package hasn't been uncompressed yet.  Do so.
    print "Uncompressing ICM code...";
    print OUT "Uncompressing ICM code...\n\n";
    close OUT;
    system("tar -zxvf $Bin/scripts/icmCode/glimmer3_plusSS.tgz -C " . 
	   "$Bin/scripts/icmCode >> $buildICMsLog 2>> $buildICMsErr");
    open OUT, ">>$buildICMsLog"
      or die("FATAL: Can't open $buildICMsLog for appending.\n");
    print "done.\n\n";
    print OUT "\ndone.\n\n";
  }
  # Next, resolve a conflict based on mutually exclusive syntax requirements 
  # across the gcc 4.3/4.4 barrier.
  open( CMD, "(gcc -v) 2>&1 |" );
  while ( my $line = <CMD> ) {
    if ( $line =~ /gcc\s+version\s+(\S+)/ ) {
      my $versionString = $1;
      my @versionNums = ();
      while ( $versionString =~ /^(\d+)\./ ) {
	my $nextNum = $1;
	push @versionNums, $nextNum;
	$versionString =~ s/^$nextNum\.//;
      }
      if ( $versionString =~ /^(\d+)/ ) {
	push @versionNums, $1;
      }
      my $topNum = $versionNums[0];
      my $secondNum = -1;
      if ( $#versionNums > 0 ) {
	$secondNum = $versionNums[1];
      }
      my $newGCC = 0;
      if ( $topNum > 4 or ( $topNum == 4 and $secondNum >= 4 ) ) {
	$newGCC = 1;
      }
      # Rename the appropriate files, if we haven't already done this.
      if ( $newGCC == 0 ) {
	print OUT "Using source files for GCC v[<=4.3.*].\n";
	# We've got an older version of gcc.
	if ( not -e "$Bin/scripts/icmCode/SimpleMake/icm.cc" ) {
	  system("cp $Bin/scripts/icmCode/SimpleMake/icm_oldgcc.cc " . 
		 "$Bin/scripts/icmCode/SimpleMake/icm.cc");
	  system("cp $Bin/scripts/icmCode/SimpleMake/gene_oldgcc.cc " . 
		 "$Bin/scripts/icmCode/SimpleMake/gene.cc");
	  system("cp $Bin/scripts/icmCode/SimpleMake/delcher_oldgcc.hh " . 
		 "$Bin/scripts/icmCode/SimpleMake/delcher.hh");
	}
      }
      else {
	print OUT "Using source files for GCC v[>=4.4.*].\n";
	# We've got a newer version of gcc.
	if ( not -e "$Bin/scripts/icmCode/SimpleMake/icm.cc" ) {
	  system("cp $Bin/scripts/icmCode/SimpleMake/icm_newgcc.cc " . 
		 "$Bin/scripts/icmCode/SimpleMake/icm.cc");
	  system("cp $Bin/scripts/icmCode/SimpleMake/gene_newgcc.cc " . 
		 "$Bin/scripts/icmCode/SimpleMake/gene.cc");
	  system("cp $Bin/scripts/icmCode/SimpleMake/delcher_newgcc.hh " . 
		 "$Bin/scripts/icmCode/SimpleMake/delcher.hh");
	}
      }
    }
  }
  close CMD;
  # Now we can compile.
  if ( not -e "$Bin/scripts/icmCode/bin/build-icm" ) {
    # The code hasn't been compiled yet.  Do so.
    print "Compiling ICM code...";
    print OUT "Compiling ICM code...\n\n";
    close OUT;
    my $currdir = getcwd;
    my $absBuildICMsLog = File::Spec->rel2abs($buildICMsLog);
    my $absBuildICMsErr = File::Spec->rel2abs($buildICMsErr);
    chdir("$Bin/scripts/icmCode/SimpleMake");
    system("make >> $absBuildICMsLog 2>> $absBuildICMsErr");
    chdir($currdir);
    open OUT, ">>$buildICMsLog"
      or die("FATAL: Can't open $buildICMsLog for appending.\n");
    print "done.\n\n";
    print OUT "\ndone.\n\n";
  }
  # Sanity: check for one of the more critical binaries.
  if ( not -e "$Bin/scripts/icmCode/bin/build-icm" ) {
    die("FATAL: The ICM code does not appear to have compiled successfully." . 
	"\n\nPlease try to compile manually (cd " . 
	"$Bin/scripts/icmCode/SimpleMake;" .
	" make);\nif that doesn't work, check to make sure you have\nthe " . 
	"appropriate version of gcc (gcc -v) for the version of Phymm" . 
	"\nthat you've downloaded (see website for details).\n");
  }
  # If ICMs haven't yet been built, build them.
  close OUT;
  system("$Bin/scripts/buildICMs.pl $genomedata $buildICMsLog $buildICMsErr " . 
	 "$nthreads");
}

# Build a local BLAST database from the RefSeq FASTA files.
sub generateBlastDB {
  my ($datapath) = @_;
 
  my $locateBlastBinary = `which makeblastdb 2>&1`;
  die("FATAL: You must have a local copy of the BLAST software installed " . 
      "and accessible via your \"PATH\" environment variable.  Please see " .
      "the README file for details.\n\n") 
    if (   $locateBlastBinary =~ /no\s+makeblastdb\s+in/
	   or $locateBlastBinary =~ /command\s+not\s+found/i );
  # Standalone BLAST is installed.  Do we need to regenerate the BLAST DB?  
  # If there's no DB, generate one.
  my $regenDB = 0;
  if ( not -e "$datapath/blastData/phymm_BLAST_DB.nal" ) {
    $regenDB = 1;
  } else {
    # If there is one, search the lastmod timestamps of the .fna files in 
    # genomeData. If we find one that's newer than the blast DB, regenerate 
    # the DB. We won't check .fna files in genomeData/.userAdded here, 
    # because if the user has used addCustomGenome.pl, then they've been 
    # repeatedly instructed to remember to rebuild the BLAST DB when they 
    # were done (if they selected the option to do it by hand in the first 
    # place, that is; by default, it's rebuilt automatically).  If they 
    # decided not to do it despite repeated cautions, well then who knows, 
    # they might have their reasons.
    my $blastTimeStamp = ( stat("$datapath/blastData/phymm_BLAST_DB.nal") )[9];
    my $genomeDir = "$datapath/genomeData";
    opendir DOT, $genomeDir
      or die("FATAL: Can't open $genomeDir for scanning.\n");
    my @subs = grep { !/^(\.)|(_userAdded)/ } readdir DOT;
    closedir DOT;
  OUTER: foreach my $sub (@subs) {
      my $fullSub = "$genomeDir/$sub";
      if ( -d $fullSub ) {
	opendir DOT, $fullSub
	  or die("FATAL: Can't open $fullSub for scanning.\n");
	my @files = grep { /\.fna$/ } readdir DOT;
	closedir DOT;
	foreach my $file (@files) {
	  my $fullFile = "$fullSub/$file";
	  my $currentTimeStamp = ( stat($fullFile) )[9];
	  if ( $currentTimeStamp > $blastTimeStamp ) {
	    # Found one.  Stop looking.
	    $regenDB = 1;
	    last OUTER;
	  }
	}
      }
    }
  }
  if ($regenDB) {
    my $blastLog = "$logDir/12_generate_BLAST_DB__out.txt";
    if ( $newCopy == 2 ) {
      $blastLog =~ s/12/16/;
    }
    open LOG, ">$blastLog"
      or die("FATAL: Can't open $blastLog for writing.\n");
    my $blastErr = "$logDir/13_generate_BLAST_DB__err.txt";
    if ( $newCopy == 2 ) {
      $blastErr =~ s/13/17/;
    }
    system("rm -f $blastErr");
    # Concatenate all genome files and build a local BLAST DB from them.
    print "Generating local BLAST database from genome data...";
    print LOG "Generating local BLAST database from genome data...\n\n";
    # First, grab a list of relative paths to all the RefSeq genomic FASTA 
    #files.
    my @fastaFiles = ();
    opendir DOT, "$datapath/genomeData"
      or die("FATAL: Can't open $datapath/genomeData for scanning.\n");
    my @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DOT;
    closedir DOT;
    foreach my $sub (@subs) {
      if ( -d "$datapath/genomeData/$sub" ) {
	opendir DOT, "$datapath/genomeData/$sub"
	  or die("FATAL: Can't open $datapath/genomeData/$sub for scanning.\n");
	my @files =
	  sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;
	closedir DOT;
	foreach my $file (@files) {
	  push @fastaFiles, "$datapath/genomeData/$sub/$file";
	}
      }
    }
    # Next, grab a list of relative paths to any user-added FASTA files.
    my $userDir = "$datapath/genomeData/_userAdded";
    if ( -e $userDir ) {
      opendir DOT, $userDir
	or die("FATAL: Can't open $userDir for scanning.\n");
      @subs = grep { !/^(\.)|(_userAdded)/ } sort { $a cmp $b } readdir DOT;
      closedir DOT;
      foreach my $sub (@subs) {
	if ( -d "$userDir/$sub" ) {
	  opendir DOT, "$userDir/$sub"
	    or die("FATAL: Can't open $userDir/$sub for scanning.\n");
	  my @files = sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;
	  closedir DOT;
	  foreach my $file (@files) {
	    push @fastaFiles, "$userDir/$sub/$file";
	  }
	}
      }
    }
    # Concatenate all the FASTA files together for database creation.
    my $command = 'cat ';
    my $argCount = 0;
    my $wrapped = 0;
    my $fileIndex = -1;
    foreach my $file (@fastaFiles) {
      $fileIndex++;
      $command .= "$file \\\n";
      $argCount++;
      if ( $argCount % 10 == 0 and $fileIndex != $#fastaFiles ) {
	if ( not $wrapped ) {
	  $wrapped = 1;
	  $command .= "> blastDB_data.txt\n";
	} else {
	  $command .= ">> blastDB_data.txt\n";
	}
	$command .= 'cat ';
      }
    }
    if ( not $wrapped ) {
      $command .= "> blastDB_data.txt\n";
    } else {
      $command .= ">> blastDB_data.txt\n";
    }
    # There's a problem with using system() for a command this long, 
    # apparently, so we use a hack.
    my $shLoc = `which sh`;
    chomp $shLoc;
    my $outFile = "command_temp.sh";
    open OUT, ">$outFile"
      or die("FATAL: Can't open $outFile for writing.\n");
    print OUT "#!$shLoc\n\n$command";
    close OUT;
    system("chmod 775 command_temp.sh");
    system("./command_temp.sh");
    system("rm command_temp.sh");
    # Create the local database copy.
    close LOG;
    system("makeblastdb -in blastDB_data.txt -title phymm_BLAST_DB -out phymm_BLAST_DB -dbtype nucl >> $logDir/12_generate_BLAST_DB__out.txt 2>> $logDir/13_generate_BLAST_DB__err.txt") == 0 
      or die "Something went wrong while running makeblastdb: $?\n";
    system("mkdir -p $datapath/blastData") == 0 
      or die ("Could not mkdir $datapath/blastData") 
	unless (-e "$datapath/blastData");
    system("mv phymm_BLAST_DB.* $datapath/blastData");
    system("rm blastDB_data.txt");
    print "done.\n\n";
    open LOG, ">>$blastLog"
      or die("FATAL: Can't open $blastLog for appending.\n");
    print LOG "\ndone.\n\n";
    close LOG;
  }    # end if ( we're generating a DB at all )
}
sub toDirName {
  my $name = shift;
  my $result = $name;
  $result =~ s/\_/UNDERSCORE/g;
  $result =~ s/\+/PLUS/g;
  $result =~ s/\s/\_/g;
  $result =~ s/\//SLASH/g;
  $result =~ s/\(/LPAREN/g;
  $result =~ s/\)/RPAREN/g;
  $result =~ s/\'/SINGLEQUOTE/g;
  $result =~ s/\"/DOUBLEQUOTE/g;
  $result =~ s/\:/COLONCOLON/g;
  $result =~ s/\;/SEMICOLON/g;
  return $result;
}
sub fromDirName {
  my $name = shift;
  my $result = $name;
  $result =~ s/\_/ /g;
  $result =~ s/UNDERSCORE/\_/g;
  $result =~ s/PLUS/\+/g;
  $result =~ s/SLASH/\//g;
  $result =~ s/LPAREN/\(/g;
  $result =~ s/RPAREN/\)/g;
  $result =~ s/SINGLEQUOTE/\'/g;
  $result =~ s/DOUBLEQUOTE/\"/g;
  $result =~ s/COLONCOLON/\:/g;
  $result =~ s/SEMICOLON/\;/g;
  return $result;
}
sub loadGBerrors {
  my $errors = shift;
  my $errorFile = 'taxonomyData/knownGBerrors.txt';
  open IN, "<$errorFile"
    or die("FATAL: Can't open $errorFile for reading.\n");
  while ( my $line = <IN> ) {
    chomp $line;
    if ( $line =~ /\t/ ) {
      ( my $canonName, my $pattern ) = split( /\t/, $line );
      $errors->{$pattern} = $canonName;
    }
  }
  close IN;
}
sub fixOrgName {
  my $orgName = shift;
  my $errors = shift;
  foreach my $pattern ( keys %$errors ) {
    my $correctName = $errors->{$pattern};
    if ( $orgName =~ /$pattern/ ) {
      $orgName = $correctName;
      return $orgName;
    }
  }
  return $orgName;
}
sub updateGenomeData {
  # Classify each genome directory in the new RefSeq set as one of following:
  #    [new organism added since existing installation was last updated],
  #    [organism contained in existing installation, but /hasn't/ been changed],
  #    [organism contained in existing installation and /has/ been changed].
  my $updateLog = "$logDir/08_update_genome_data__out.txt";
  open OUT, ">$updateLog"
    or die("FATAL: Can't open $updateLog for writing.\n");
  my $updateErr = "$logDir/09_update_genome_data__err.txt";
  open ERR, ">$updateErr"
    or die("FATAL: Can't open $updateErr for writing.\n");
  # Load the existing genome directory list, along with .fna file sizes.
  my $extantGenomes = {};
  opendir GENOMEDATA, 'genomeData'
    or die("FATAL: Can't open genomeData for scanning.\n");
  my @subs = readdir GENOMEDATA;
  closedir GENOMEDATA;
  foreach my $sub (@subs) {
    if ( -d "genomeData/$sub" and $sub !~ /^\./ ) {
      opendir DOT, "genomeData/$sub"
	or die("FATAL: Can't open genomeData/$sub for scanning.\n");
      my @files = readdir DOT;
      closedir DOT;
      foreach my $file (@files) {
	if ( $file =~ /\.fna$/ ) {
	  my $fileSize = -s "genomeData/$sub/$file";
	  $extantGenomes->{$sub}->{$file} = $fileSize;
	}
      }
    }
  }
  # Create .fna files for the new download.
  print "Creating temp FASTA files from new .gbk files for update check...";
  print OUT 
    "Creating temp FASTA files from new .gbk files for update check...\n\n";
  close OUT;
  close ERR;
  system("$Bin/scripts/genbankToFasta.pl _tempGenomeData >> $updateLog " . 
	 "2>> $updateErr");
  open OUT, ">>$updateLog"
    or die("FATAL: Can't open $updateLog for appending.\n");
  open ERR, ">>$updateErr"
    or die("FATAL: Can't open $updateErr for appending.\n");
  print "done.\n\n";
  print OUT "done.\n\n";
  # Load the new genome directory list, along with .fna file sizes.
  my $newGenomes = {};
  opendir GENOMEDATA, '_tempGenomeData'
    or die("FATAL: Can't open _tempGenomeData for scanning.\n");
  @subs = readdir GENOMEDATA;
  closedir GENOMEDATA;
  foreach my $sub (@subs) {
    if ( -d "_tempGenomeData/$sub" and $sub !~ /^\./ ) {
      opendir DOT, "_tempGenomeData/$sub"
	or die("FATAL: Can't open _tempGenomeData/$sub for scanning.\n");
      my @files = readdir DOT;
      closedir DOT;
      foreach my $file (@files) {
	if ( $file =~ /\.fna$/ ) {
	  my $fileSize = -s "_tempGenomeData/$sub/$file";
	  $newGenomes->{$sub}->{$file} = $fileSize;
	}
      }
    }
  }
  # First, find the completely new critters, if any.
  my $addedGenomeHash = {};
  foreach my $newGenome ( keys %$newGenomes ) {
    if ( not exists( $extantGenomes->{$newGenome} ) ) {
      $addedGenomeHash->{$newGenome} = 1;
    }
  }
  my @addedGenomes = sort { $a cmp $b } keys %$addedGenomeHash;
  if ( $#addedGenomes == -1 ) {
    print "No new genomes to add.\n\n";
    print OUT "No new genomes to add.\n\n";
  }
  else {
    print "Adding new genomes:\n\n";
    print OUT "Adding new genomes:\n\n";
    foreach my $newGenome (@addedGenomes) {
      print "   $newGenome\n";
      print OUT "   $newGenome\n";
    }
    print "\n";
    print OUT "\n";
  }
  # Next, identify existing genomes which need to be updated, if any.
  my $updatedGenomeHash = {};
  foreach my $newGenome ( keys %$newGenomes ) {
    foreach my $file ( keys %{ $newGenomes->{$newGenome} } ) {
      if (   not exists( $extantGenomes->{$newGenome} )
	     or not exists( $extantGenomes->{$newGenome}->{$file} )
	     or $extantGenomes->{$newGenome}->{$file} !=
	     $newGenomes->{$newGenome}->{$file} )
	{
	  if ( not exists( $addedGenomeHash->{$newGenome} ) ) {
	    $updatedGenomeHash->{$newGenome}->{$file} = 1;
	  }
	}
    }
  }
  my @updatedGenomes = sort { $a cmp $b } keys %$updatedGenomeHash;
  if ( $#updatedGenomes == -1 ) {
    print "No updates found for existing genomes.\n\n";
    print OUT "No updates found for existing genomes.\n\n";
  }
  else {
    print "Updating existing genomes with new data:\n\n";
    print OUT "Updating existing genomes with new data:\n\n";
    foreach my $updateGenome (@updatedGenomes) {
      print "   $updateGenome\n";
      print OUT "   $updateGenome\n";
    }
    print "\n";
    print OUT "\n";
  }
  # Process the additions: just move the directories over.
  print OUT "Processing new genomes...";
  foreach my $addGenome (@addedGenomes) {
    system("cp -R _tempGenomeData/$addGenome genomeData");
  }
  print OUT "done.\n";
  # Process the updates: remove old .gbk files, as well
  # as any files generated from them, and copy the new
  # .gbk files over.
  print OUT "Processing existing-genome updates...";
  foreach my $updateGenome (@updatedGenomes) {
    my @updateFiles = keys %{ $updatedGenomeHash->{$updateGenome} };
    foreach my $updateFile (@updateFiles) {
      $updateFile =~ /^(.+)\.fna/;
      my $prefix = $1;
      my $command = "rm -rf genomeData/$updateGenome/$prefix\*";
      system($command);
      $command = "cp _tempGenomeData/$updateGenome/$prefix\.gbk " . 
	"genomeData/$updateGenome/$prefix\.gbk";
      system($command);
    }
  }
  print OUT "done.\n";
  # Delete the temporary genome repository; we're done with it.
  print OUT "Deleting temporary genome repository...";
  system("rm -rf _tempGenomeData");
  print OUT "done.\n";
  close OUT;
  close ERR;
}
sub unixEscape {
  my $arg = shift;
  $arg =~ s/\(/\\\(/g;
  $arg =~ s/\)/\\\)/g;
  $arg =~ s/\'/\\\'/g;
  $arg =~ s/\"/\\\"/g;
  $arg =~ s/ /\\ /g;
  return $arg;
}
